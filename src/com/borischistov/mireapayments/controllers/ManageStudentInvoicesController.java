package com.borischistov.mireapayments.controllers;

import com.borischistov.mireapayments.entities.Contract;
import com.borischistov.mireapayments.entities.Invoice;
import com.borischistov.mireapayments.entities.StudySemester;
import com.borischistov.mireapayments.services.api.IContractService;
import com.borischistov.mireapayments.services.api.IInvoiceService;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: b_chistov
 * Date: 02.12.13
 * Time: 16:08
 */
@Component
@ManagedBean
@SessionScoped
public class ManageStudentInvoicesController {

    @Autowired
    private IContractService contractService;
    @Autowired
    private AuthController authController;
    @Autowired
    private IInvoiceService invoiceService;

    public List<Contract> getContractList() {
        List<Contract> result = contractService.getContracts(authController.getCurrentUser());
        return result;
    }

    public List<Invoice> getInvoices(Contract c) {
        List<Invoice> result = invoiceService.getInvoices(c);
        Collections.sort(result, new Comparator<Invoice>() {
            @Override
            public int compare(Invoice o1, Invoice o2) {
                StudySemester s1 = o1.getPlan().getStudySemester();
                StudySemester s2 = o2.getPlan().getStudySemester();
                if (s1.getStartYear() > s2.getStartYear()) {
                    return 1;
                } else if (s1.getStartYear() < s2.getStartYear()) {
                    return -1;
                } else {
                    if (s1.getSemester() > s2.getSemester()) {
                        return 1;
                    } else if (s1.getSemester() < s2.getSemester()) {
                        return -1;
                    } else {
                        return 0;
                    }
                }
            }
        });
        return result;
    }

    public void createInvoice(Invoice invoice) {
        invoice.setInvoiceCreationDate(new Date());
        invoice.setInvoiceNumber("NUMBER");
        invoiceService.save(invoice);
        downloadInvoice(invoice);
    }

    public void refreshInvoice(Invoice invoice) {
        downloadInvoice(invoice);
    }

    private void downloadInvoice(Invoice invoice) {
        try {
            ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
            ec.responseReset();
            ec.setResponseHeader("Content-Type", ec.getMimeType("invoices-template.docx"));
            ec.setResponseHeader("Content-Disposition", "attachment;filename=\"" + "invoice.docx" + "\"");
            XWPFDocument doc =
                    new XWPFDocument(OPCPackage.open(
                            ec.getResourceAsStream("/resources/templates/invoice-template.docx")));
            doc = invoiceService.prepareInvoiceFile(invoice, doc);
            OutputStream outputStream = ec.getResponseOutputStream();
            doc.write(outputStream);
            outputStream.close();
            FacesContext.getCurrentInstance().responseComplete();
        } catch (IOException e) {

        } catch (InvalidFormatException e) {

        }

    }
}
