package com.borischistov.mireapayments.controllers;

import com.borischistov.mireapayments.entities.Group;
import com.borischistov.mireapayments.services.api.IGroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Boris Chistov
 * Date: 07.11.13
 * Time: 22:26
 * Comment:
 */
@Component
@ManagedBean
@RequestScoped
public class ManageGroupController {

    @Autowired
    private IGroupService groupService;
    private Group group = new Group();

    public void reset() {
        group = new Group();
    }

    public void saveGroup() {
        groupService.save(group);
    }

    public void delete(Group group) {
        groupService.delete(group);
        reset();
    }

    public void selectGroup(Group group) {
        this.group = group;
    }

    public List<Group> getGroups() {
        return groupService.getGroups();
    }

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }
}
