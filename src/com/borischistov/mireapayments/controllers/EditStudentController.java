package com.borischistov.mireapayments.controllers;

import com.borischistov.mireapayments.entities.Student;
import com.borischistov.mireapayments.entities.utilis.PersonType;
import com.borischistov.mireapayments.services.api.IStudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Component;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import java.util.ResourceBundle;

/**
 * Created with IntelliJ IDEA.
 * User: Boris Chistov
 * Date: 04.11.13
 * Time: 20:26
 * Comment:
 */
@Component
@ManagedBean
@RequestScoped
public class EditStudentController {

    @Autowired
    private IStudentService studentService;
    private Student student = new Student();

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public void reset() {
        student = new Student();
    }

    public void saveStudent() {
        try {
            student.setType(PersonType.STUDENT);
            studentService.saveStudent(student);
        } catch (DuplicateKeyException e) {
            FacesContext context = FacesContext.getCurrentInstance();
            ResourceBundle bundle = context.getApplication().getResourceBundle(context, "editStudentMsg");
            FacesMessage facesMessage = new FacesMessage();
            facesMessage.setSummary(bundle.getString("DuplicateKeyErrorSum"));
            facesMessage.setSeverity(FacesMessage.SEVERITY_WARN);
            facesMessage.setDetail(bundle.getString("DuplicateKeyErrorDetailsStud"));
            context.addMessage(null, facesMessage);

        } catch (DataAccessException e) {
            e.printStackTrace();
            FacesContext context = FacesContext.getCurrentInstance();
            ResourceBundle bundle = context.getApplication().getResourceBundle(context, "editStudentMsg");
            FacesMessage facesMessage = new FacesMessage();
            facesMessage.setSummary(bundle.getString("OtherErrorSum"));
            facesMessage.setSeverity(FacesMessage.SEVERITY_WARN);
            facesMessage.setDetail(bundle.getString("OtherErrorDetails"));
            context.addMessage(null, facesMessage);
        }
    }
}
