package com.borischistov.mireapayments.controllers;

import com.borischistov.mireapayments.controllers.utils.MenuItem;
import org.springframework.stereotype.Component;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: b_chistov
 * Date: 05.11.13
 * Time: 14:55
 */
@Component
@ManagedBean
@RequestScoped
public class MenuController {

    private List<MenuItem> studyMenus;
    private List<MenuItem> financeMenus;

    public MenuController() {
        studyMenus = new ArrayList<MenuItem>();
        studyMenus.add(new MenuItem("MenuItemManageStudents", "/Admins/manageStudentsView.xhtml"));
        studyMenus.add(new MenuItem("MenuItemManageForm", "/Admins/manageFormView.xhtml"));
        studyMenus.add(new MenuItem("MenuItemManageSpeciality", "/Admins/manageSpecialityView.xhtml"));
        studyMenus.add(new MenuItem("StatusTitle", "/Admins/manageStatusView.xhtml"));
        studyMenus.add(new MenuItem("GroupTitle", "/Admins/manageGroupView.xhtml"));
        studyMenus.add(new MenuItem("StudySemesterTitle", "/Admins/manageStudySemesterView.xhtml"));

        financeMenus = new ArrayList<MenuItem>();
        financeMenus.add(new MenuItem("MenuItemManageContracts", "/Admins/manageContractsView.xhtml"));
        financeMenus.add(new MenuItem("MenuItemManagePaymentPlan", "/Admins/managePaymentPlanView.xhtml"));
    }

    public List<MenuItem> getStudyMenus() {
        return studyMenus;
    }

    public List<MenuItem> getFinanceMenus() {
        return financeMenus;
    }
}
