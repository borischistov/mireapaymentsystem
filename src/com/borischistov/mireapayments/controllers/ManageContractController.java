package com.borischistov.mireapayments.controllers;

import com.borischistov.mireapayments.entities.Contract;
import com.borischistov.mireapayments.services.api.IContractService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: b_chistov
 * Date: 11.11.13
 * Time: 19:59
 */
@Component
@ManagedBean
@RequestScoped
public class ManageContractController {

    @Autowired
    private IContractService contractService;

    @Autowired
    private EditStudentController editStudentController;

    public List<Contract> getContractsByStudent() {
        if (editStudentController.getStudent() != null) {
            return contractService.getContracts(editStudentController.getStudent());
        } else {
            return null;
        }
    }

    public List<Contract> getContracts() {
        return contractService.getContracts();
    }
}
