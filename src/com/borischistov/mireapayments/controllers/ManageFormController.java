package com.borischistov.mireapayments.controllers;

import com.borischistov.mireapayments.entities.Form;
import com.borischistov.mireapayments.services.api.IFormService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Component;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: b_chistov
 * Date: 06.11.13
 * Time: 9:32
 */
@Component
@ManagedBean
@RequestScoped
public class ManageFormController {

    @Autowired
    private IFormService formService;

    private Form form;

    public ManageFormController() {
        this.form = new Form();
    }

    public void saveForm() {
        System.out.println(form.getName() + " " + form.getId());
        try {
            formService.saveForm(form);
        } catch (DuplicateKeyException e) {

        }
    }

    public void deleteForm(Form form) {
        formService.delete(form);
        this.form = new Form();
    }

    public void reset() {
        form = new Form();
    }

    public Form getForm() {
        return form;
    }

    public void selectForm(Form form) {
        this.form = form;
    }

    public List<Form> getForms() {
        return formService.getForms();
    }
}
