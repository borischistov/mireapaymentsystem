package com.borischistov.mireapayments.controllers;

import com.borischistov.mireapayments.entities.Student;
import com.borischistov.mireapayments.services.api.IStudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: b_chistov
 * Date: 05.11.13
 * Time: 11:32
 */
@Component
@ManagedBean
@RequestScoped
public class ManageStudentsController {

    @Autowired
    private IStudentService studentService;

    public List<Student> getStudents() {
        return studentService.getStudents();
    }
}
