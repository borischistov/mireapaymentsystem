package com.borischistov.mireapayments.controllers;

import com.borischistov.mireapayments.entities.Speciality;
import com.borischistov.mireapayments.services.api.ISpecialityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: b_chistov
 * Date: 07.11.13
 * Time: 15:44
 */
@Component
@ManagedBean
@RequestScoped
public class ManageSpecialityController {

    @Autowired
    private ISpecialityService specialityService;
    private Speciality speciality = new Speciality();

    public List<Speciality> getSpecialities() {
        return specialityService.getSpecialities();
    }

    public void reset() {
        this.speciality = new Speciality();
    }

    public Speciality getSpeciality() {
        return speciality;
    }

    public void setSpeciality(Speciality speciality) {
        this.speciality = speciality;
    }

    public void saveSpeciality() {
        specialityService.saveSpeciality(speciality);
    }

    public void selectSpeciality(Speciality speciality) {
        this.speciality = speciality;
    }

    public void delete(Speciality speciality) {
        specialityService.delete(speciality);
        this.speciality = new Speciality();
    }
}
