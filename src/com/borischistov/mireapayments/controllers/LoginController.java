package com.borischistov.mireapayments.controllers;

import com.borischistov.mireapayments.controllers.utils.NavUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;

/**
 * Created with IntelliJ IDEA.
 * User: Boris Chistov
 * Date: 30.11.13
 * Time: 21:55
 * Comment:
 */
@Component
@ManagedBean
@RequestScoped
public class LoginController {

    private String login;
    private String password;

    @Autowired
    private AuthenticationManager simpleAuthManager;

    public String doLogin() {
        try {
            Authentication request = new UsernamePasswordAuthenticationToken(getLogin(), getPassword());
            Authentication result = simpleAuthManager.authenticate(request);
            SecurityContextHolder.getContext().setAuthentication(result);
            if (result.getAuthorities().contains(new SimpleGrantedAuthority("ROLE_ADMIN"))) {
                return NavUtils.ADMIN;
            } else if (result.getAuthorities().contains(new SimpleGrantedAuthority("ROLE_STUDENT"))) {
                return NavUtils.STUDENT;
            }
        } catch (BadCredentialsException e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Ошибка авторизации", "Не верный пароль"));
        } catch (AuthenticationServiceException e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Ошибка авторизации", "Не верные данные учетной записи"));
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Ошибка не определена", "Обратитесь к разработчику ПО"));
        }
        return NavUtils.FAIL;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
