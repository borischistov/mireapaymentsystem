package com.borischistov.mireapayments.controllers.utils.converters;

import com.borischistov.mireapayments.entities.Form;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: b_chistov
 * Date: 11.11.13
 * Time: 16:35
 */
@FacesConverter("formConverter")
public class FormConverter implements Converter {

    private static Map<Integer, Form> list;

    public FormConverter() {
        if (list == null) {
            list = new HashMap<Integer, Form>();
        }
    }

    @Override
    public Object getAsObject(FacesContext facesContext, UIComponent uiComponent, String s) {
        return list.get(Integer.parseInt(s));
    }

    @Override
    public String getAsString(FacesContext facesContext, UIComponent uiComponent, Object o) {
        Form form = (Form) o;
        list.put(form.getId(), form);
        return String.valueOf(form.getId());
    }
}
