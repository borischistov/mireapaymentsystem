package com.borischistov.mireapayments.controllers.utils.converters;

import com.borischistov.mireapayments.entities.PaymentPlan;
import com.borischistov.mireapayments.services.api.IPaymentPlanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

/**
 * Created with IntelliJ IDEA.
 * User: b_chistov
 * Date: 25.11.13
 * Time: 10:39
 */
@Component
@ManagedBean
@RequestScoped
public class PaymentPlanParamConverter implements Converter {

    @Autowired
    private IPaymentPlanService paymentPlanService;

    @Override
    public Object getAsObject(FacesContext facesContext, UIComponent uiComponent, String s) {
        PaymentPlan plan = null;
        try {
            int id = Integer.parseInt(s);
            plan = paymentPlanService.getPlans(id);
        } catch (Exception e) {

        }
        return plan;
    }

    @Override
    public String getAsString(FacesContext facesContext, UIComponent uiComponent, Object o) {
        return null;
    }
}
