package com.borischistov.mireapayments.controllers.utils.converters;

import com.borischistov.mireapayments.entities.Speciality;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: b_chistov
 * Date: 11.11.13
 * Time: 17:27
 */
@FacesConverter("specialityConverter")
public class SpecialityConverter implements Converter {

    private static Map<Integer, Speciality> list;

    public SpecialityConverter() {
        if (list == null) {
            list = new HashMap<Integer, Speciality>();
        }
    }

    @Override
    public Object getAsObject(FacesContext facesContext, UIComponent uiComponent, String s) {
        return list.get(Integer.parseInt(s));
    }

    @Override
    public String getAsString(FacesContext facesContext, UIComponent uiComponent, Object o) {
        Speciality speciality = (Speciality) o;
        list.put(speciality.getId(), speciality);
        return String.valueOf(speciality.getId());
    }
}
