package com.borischistov.mireapayments.controllers.utils.converters;

import com.borischistov.mireapayments.entities.Status;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: b_chistov
 * Date: 11.11.13
 * Time: 17:29
 */
@FacesConverter("statusConverter")
public class StatusConverter implements Converter {

    private static Map<Integer, Status> list;

    public StatusConverter() {
        if (list == null) {
            list = new HashMap<Integer, Status>();
        }
    }

    @Override
    public Object getAsObject(FacesContext facesContext, UIComponent uiComponent, String s) {
        return list.get(Integer.parseInt(s));
    }

    @Override
    public String getAsString(FacesContext facesContext, UIComponent uiComponent, Object o) {
        Status status = (Status) o;
        list.put(status.getId(), status);
        return String.valueOf(status.getId());
    }
}
