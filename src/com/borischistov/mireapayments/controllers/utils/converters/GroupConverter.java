package com.borischistov.mireapayments.controllers.utils.converters;

import com.borischistov.mireapayments.entities.Group;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: b_chistov
 * Date: 11.11.13
 * Time: 14:36
 */
@FacesConverter("groupConverter")
public class GroupConverter implements Converter {

    private static Map<Integer, Group> list;

    public GroupConverter() {
        if (list == null) {
            list = new HashMap<Integer, Group>();
        }
    }

    @Override
    public Object getAsObject(FacesContext facesContext, UIComponent uiComponent, String s) {
        return list.get(Integer.valueOf(s));
    }

    @Override
    public String getAsString(FacesContext facesContext, UIComponent uiComponent, Object o) {
        Group group = (Group) o;
        list.put(group.getId(), group);
        return String.valueOf(group.getId());
    }
}
