package com.borischistov.mireapayments.controllers.utils.converters;

import com.borischistov.mireapayments.entities.utilis.Money;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

/**
 * Created with IntelliJ IDEA.
 * User: b_chistov
 * Date: 25.11.13
 * Time: 12:24
 */
@FacesConverter("moneyConverter")
public class MoneyConverter implements Converter {

    @Override
    public Object getAsObject(FacesContext facesContext, UIComponent uiComponent, String s) {
        double amount = Double.parseDouble(s);
        return new Money(amount);
    }

    @Override
    public String getAsString(FacesContext facesContext, UIComponent uiComponent, Object o) {
        if (o != null && o instanceof Money) return Double.toString(((Money) o).getAmount());
        return null;
    }
}
