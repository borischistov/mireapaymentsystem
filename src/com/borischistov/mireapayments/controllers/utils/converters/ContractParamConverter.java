package com.borischistov.mireapayments.controllers.utils.converters;

import com.borischistov.mireapayments.entities.Contract;
import com.borischistov.mireapayments.services.api.IContractService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Component;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

/**
 * Created with IntelliJ IDEA.
 * User: b_chistov
 * Date: 12.11.13
 * Time: 13:11
 */
@Component
@ManagedBean
@RequestScoped
public class ContractParamConverter implements Converter {

    @Autowired
    private IContractService contractService;

    @Override
    public Object getAsObject(FacesContext facesContext, UIComponent uiComponent, String s) {
        Contract result = null;
        int id = Integer.parseInt(s);
        if (id <= 0) {

        } else {
            try {
                result = contractService.getContracts(id);
            } catch (DataAccessException e) {

            }
        }
        return result;
    }

    @Override
    public String getAsString(FacesContext facesContext, UIComponent uiComponent, Object o) {
        return null;
    }
}
