package com.borischistov.mireapayments.controllers.utils.converters;

import com.borischistov.mireapayments.entities.StudySemester;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: b_chistov
 * Date: 11.11.13
 * Time: 14:20
 */
@FacesConverter("semesterConverter")
public class SemesterConverter implements Converter {

    private static Map<Integer, StudySemester> list;

    public SemesterConverter() {
        if (list == null) {
            list = new HashMap<Integer, StudySemester>();
        }
    }

    @Override
    public Object getAsObject(FacesContext facesContext, UIComponent uiComponent, String s) {
        return list.get(Integer.valueOf(s));
    }

    @Override
    public String getAsString(FacesContext facesContext, UIComponent uiComponent, Object o) {
        StudySemester semester = (StudySemester) o;
        list.put(semester.getSemesterID(), semester);
        return String.valueOf(semester.getSemesterID());
    }
}
