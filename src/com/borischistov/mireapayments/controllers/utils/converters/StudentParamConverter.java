package com.borischistov.mireapayments.controllers.utils.converters;

import com.borischistov.mireapayments.entities.Student;
import com.borischistov.mireapayments.services.api.IStudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Component;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

/**
 * Created with IntelliJ IDEA.
 * User: b_chistov
 * Date: 11.11.13
 * Time: 20:08
 */
@Component
@ManagedBean
@RequestScoped
public class StudentParamConverter implements Converter {

    @Autowired
    private IStudentService studentService;

    @Override
    public Object getAsObject(FacesContext facesContext, UIComponent uiComponent, String s) {
        Student result = new Student();
        int id = Integer.parseInt(s);
        if (id > 0) {
            try {
                result = studentService.getStudents(id);
            } catch (DataAccessException e) {

            }
        }
        return result;
    }

    @Override
    public String getAsString(FacesContext facesContext, UIComponent uiComponent, Object o) {
        if (o != null && o instanceof Student) {
            Student student = (Student) o;
            return String.valueOf(student.getId());
        }
        return null;
    }
}
