package com.borischistov.mireapayments.controllers.utils;

import org.springframework.stereotype.Component;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.NoneScoped;
import javax.faces.context.FacesContext;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: b_chistov
 * Date: 06.11.13
 * Time: 13:51
 */
@Component
@ManagedBean
@NoneScoped
public class NavUtils {

    public static final String SUCCESS = "SUCCESS";
    public static final String FAIL = "FAIL";
    public static final String BACK = "BACK";
    public static final String ADMIN = "ADMIN";
    public static final String STUDENT = "STUDENT";
    public static final String STUDENT_PRAM = "student";
    public static final String CONTRACT_PARAM = "contract";
    public static final String PAYMENT_PLAN_PARAM = "payment_plan";


    public static void setRedirect(String viewID) {
        FacesContext context = FacesContext.getCurrentInstance();
        try {
            context.getExternalContext().redirect(viewID);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String getPaymentPlanParam() {
        return PAYMENT_PLAN_PARAM;
    }

    public String getStudentPram() {
        return STUDENT_PRAM;
    }

    public String getContractParam() {
        return CONTRACT_PARAM;
    }

    public String getSuccess() {
        return SUCCESS;
    }

    public String getFail() {
        return FAIL;
    }

    public String getBack() {
        return BACK;
    }

    public static String getAdmin() {
        return ADMIN;
    }

    public static String getStudent() {
        return STUDENT;
    }
}
