package com.borischistov.mireapayments.controllers.utils;

import javax.faces.context.ExceptionHandler;
import javax.faces.context.ExceptionHandlerFactory;

/**
 * Created with IntelliJ IDEA.
 * User: b_chistov
 * Date: 09.12.13
 * Time: 11:17
 */
public class AppExceptionHandlerFactory extends ExceptionHandlerFactory {

    private ExceptionHandlerFactory parent;

    public AppExceptionHandlerFactory(ExceptionHandlerFactory parent) {
        this.parent = parent;
    }

    @Override
    public ExceptionHandler getExceptionHandler() {
        return new AppExceptionHandler(parent.getExceptionHandler());
    }
}
