package com.borischistov.mireapayments.controllers.utils;

/**
 * Created with IntelliJ IDEA.
 * User: b_chistov
 * Date: 05.11.13
 * Time: 14:53
 */
public class MenuItem {

    private String menuName;
    private String viewID;

    public MenuItem() {
    }

    public MenuItem(String menuName, String viewID) {
        this.menuName = menuName;
        this.viewID = viewID;
    }

    public String getMenuName() {
        return menuName;
    }

    public void setMenuName(String menuName) {
        this.menuName = menuName;
    }

    public String getViewID() {
        return viewID;
    }

    public void setViewID(String viewID) {
        this.viewID = viewID;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MenuItem menuItem = (MenuItem) o;

        if (menuName != null ? !menuName.equals(menuItem.menuName) : menuItem.menuName != null) return false;
        if (viewID != null ? !viewID.equals(menuItem.viewID) : menuItem.viewID != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = menuName != null ? menuName.hashCode() : 0;
        result = 31 * result + (viewID != null ? viewID.hashCode() : 0);
        return result;
    }
}
