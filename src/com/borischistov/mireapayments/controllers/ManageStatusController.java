package com.borischistov.mireapayments.controllers;

import com.borischistov.mireapayments.entities.Status;
import com.borischistov.mireapayments.services.api.IStatusService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: b_chistov
 * Date: 07.11.13
 * Time: 16:32
 */
@Component
@ManagedBean
@RequestScoped
public class ManageStatusController {

    @Autowired
    private IStatusService statusService;
    private Status status = new Status();

    public List<Status> getStatuses() {
        return statusService.getStatuses();
    }

    public void selectStatus(Status status) {
        this.status = status;
    }

    public void reset() {
        this.status = new Status();
    }

    public void save() {
        statusService.save(status);
    }

    public void delete(Status status) {
        statusService.delete(status);
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }
}
