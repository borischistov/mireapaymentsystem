package com.borischistov.mireapayments.controllers;

import com.borischistov.mireapayments.controllers.utils.NavUtils;
import com.borischistov.mireapayments.entities.Contract;
import com.borischistov.mireapayments.entities.Student;
import com.borischistov.mireapayments.services.api.IContractService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Component;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

/**
 * Created with IntelliJ IDEA.
 * User: b_chistov
 * Date: 11.11.13
 * Time: 10:15
 */
@Component
@ManagedBean
@RequestScoped
public class EditContractController {

    @Autowired
    private IContractService contractService;

    private Contract contract;
    private Student student;

    public void init() {
        if (contract == null && student == null) {
            NavUtils.setRedirect("BACK");
        } else if (contract == null) {
            contract = new Contract();
            contract.setStudent(student);
        }
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public Contract getContract() {
        return contract;
    }

    public void setContract(Contract contract) {
        this.contract = contract;
    }

    public void back() {
        contract = new Contract();
    }

    public String save() {
        try {
            contractService.save(contract);
            return NavUtils.SUCCESS;
        } catch (DataAccessException e) {
            e.printStackTrace();
        }
        return NavUtils.FAIL;
    }
}
