package com.borischistov.mireapayments.controllers;

import com.borischistov.mireapayments.entities.Person;
import com.borischistov.mireapayments.entities.utilis.AuthDetails;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

/**
 * Created with IntelliJ IDEA.
 * User: b_chistov
 * Date: 02.12.13
 * Time: 12:34
 */
@Component
@ManagedBean
@RequestScoped
public class AuthController {

    public Person getCurrentUser() {
        Person result = ((AuthDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getPerson();
        return result;
    }
}
