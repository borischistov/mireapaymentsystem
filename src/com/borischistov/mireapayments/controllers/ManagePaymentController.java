package com.borischistov.mireapayments.controllers;

import com.borischistov.mireapayments.entities.Form;
import com.borischistov.mireapayments.entities.PaymentPlan;
import com.borischistov.mireapayments.entities.Speciality;
import com.borischistov.mireapayments.entities.StudySemester;
import com.borischistov.mireapayments.services.api.IFormService;
import com.borischistov.mireapayments.services.api.IPaymentPlanService;
import com.borischistov.mireapayments.services.api.ISemesterService;
import com.borischistov.mireapayments.services.api.ISpecialityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Component;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import java.util.Date;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: b_chistov
 * Date: 25.11.13
 * Time: 10:49
 */
@Component
@ManagedBean
@RequestScoped
public class ManagePaymentController {

    private PaymentPlan paymentPlan;

    @Autowired
    private IPaymentPlanService paymentPlanService;
    @Autowired
    private ISemesterService semesterService;
    @Autowired
    private IFormService formService;
    @Autowired
    private ISpecialityService specialityService;

    public void init() {
        if (paymentPlan == null) {
            paymentPlan = new PaymentPlan();
            paymentPlan.setPaymentPlanCreationDate(new Date());
        }
    }

    public List<PaymentPlan> getPaymentPlans() {
        return paymentPlanService.getPlans();
    }

    public PaymentPlan getPaymentPlan() {
        return paymentPlan;
    }

    public void setPaymentPlan(PaymentPlan paymentPlan) {
        this.paymentPlan = paymentPlan;
    }

    public List<StudySemester> getStudySemesters() {
        return semesterService.getSemesters();
    }

    public List<Form> getForms() {
        return formService.getForms();
    }

    public List<Speciality> getSpecialities() {
        return specialityService.getSpecialities();
    }

    public void save() {
        try {
            paymentPlanService.save(paymentPlan);
            paymentPlan = new PaymentPlan();
        } catch (DataAccessException e) {

        }
    }

    public void reset() {
        paymentPlan = null;
    }

    public void select(PaymentPlan plan) {
        this.paymentPlan = plan;
    }
}
