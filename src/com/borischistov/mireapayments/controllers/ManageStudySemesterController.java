package com.borischistov.mireapayments.controllers;

import com.borischistov.mireapayments.entities.StudySemester;
import com.borischistov.mireapayments.services.api.ISemesterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Boris Chistov
 * Date: 11.11.13
 * Time: 0:02
 * Comment:
 */
@Component
@ManagedBean
@RequestScoped
public class ManageStudySemesterController {

    @Autowired
    private ISemesterService semesterService;

    private StudySemester semester = new StudySemester();

    public StudySemester getSemester() {
        return semester;
    }

    public List<StudySemester> getSemesters() {
        return semesterService.getSemesters();
    }

    public void setSemester(StudySemester semester) {
        this.semester = semester;
    }

    public void save() {
        semesterService.save(semester);
        semester = new StudySemester();
    }

    public void reset() {
        semester = new StudySemester();
    }

    public void selectSemester(StudySemester semester) {
        this.semester = semester;
    }

    public void delete(StudySemester semester) {
        semesterService.delete(semester);
    }
}
