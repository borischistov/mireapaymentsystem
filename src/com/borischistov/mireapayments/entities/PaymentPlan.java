package com.borischistov.mireapayments.entities;

import com.borischistov.mireapayments.entities.utilis.Money;

import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: Boris Chistov
 * Date: 04.11.13
 * Time: 11:10
 * Comment: Описывает сущность плана оплаты.
 */
public class PaymentPlan {

    private Money paymentSum;
    private Date paymentPlanCreationDate;
    private Form form;
    private StudySemester studySemester;
    private Speciality speciality;
    private int id;

    public PaymentPlan() {
    }

    public PaymentPlan(Money paymentSum, Date paymentPlanCreationDate,
                       Form form, StudySemester studySemester,
                       Speciality speciality, int id) {
        this.paymentSum = paymentSum;
        this.paymentPlanCreationDate = paymentPlanCreationDate;
        this.form = form;
        this.studySemester = studySemester;
        this.speciality = speciality;
        this.id = id;
    }

    public Money getPaymentSum() {
        return paymentSum;
    }

    public void setPaymentSum(Money paymentSum) {
        this.paymentSum = paymentSum;
    }

    public Date getPaymentPlanCreationDate() {
        return paymentPlanCreationDate;
    }

    public void setPaymentPlanCreationDate(Date paymentPlanCreationDate) {
        this.paymentPlanCreationDate = paymentPlanCreationDate;
    }

    public Form getForm() {
        return form;
    }

    public void setForm(Form form) {
        this.form = form;
    }

    public StudySemester getStudySemester() {
        return studySemester;
    }

    public void setStudySemester(StudySemester studySemester) {
        this.studySemester = studySemester;
    }

    public Speciality getSpeciality() {
        return speciality;
    }

    public void setSpeciality(Speciality speciality) {
        this.speciality = speciality;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
