package com.borischistov.mireapayments.entities.utilis;

import com.borischistov.mireapayments.entities.Person;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

/**
 * Created with IntelliJ IDEA.
 * User: b_chistov
 * Date: 28.11.13
 * Time: 13:20
 */
public class AuthDetails implements UserDetails {

    private int id;
    private String login;
    private String password;
    private Collection<GrantedAuthority> authorities;
    private Person person;
    private static final String AUTH_DELIMITER = " / ";

    public AuthDetails(String login, String password, String authorities) {
        this.login = login;
        this.password = password;
        this.authorities = new ArrayList<GrantedAuthority>();
        for (String auth : authorities.split(AUTH_DELIMITER)) {
            this.authorities.add(new SimpleGrantedAuthority(auth));
        }
    }

    public AuthDetails(String login, String password, String authorities, Person person) {
        this.login = login;
        this.password = password;
        this.authorities = new ArrayList<GrantedAuthority>();
        for (String auth : authorities.split(AUTH_DELIMITER)) {
            this.authorities.add(new SimpleGrantedAuthority(auth));
        }
        this.person = person;
    }

    public AuthDetails(int id, String login, String password, String authorities) {
        this.id = id;
        this.login = login;
        this.password = password;
        this.authorities = new ArrayList<GrantedAuthority>();
        for (String auth : authorities.split(AUTH_DELIMITER)) {
            this.authorities.add(new SimpleGrantedAuthority(auth));
        }
    }

    public AuthDetails(int id, String login, String password, String authorities, Person person) {
        this.id = id;
        this.login = login;
        this.password = password;
        this.authorities = new ArrayList<GrantedAuthority>();
        for (String auth : authorities.split(AUTH_DELIMITER)) {
            this.authorities.add(new SimpleGrantedAuthority(auth));
        }
        this.person = person;
    }

    public Person getPerson() {
        return person;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getAuthoritiesAsString() {
        StringBuilder sb = new StringBuilder();
        Iterator<GrantedAuthority> it = authorities.iterator();
        while (it.hasNext()) {
            sb.append(it.next().getAuthority());
            if (it.hasNext()) sb.append(AUTH_DELIMITER);
        }
        return sb.toString();
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return login;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    @Override
    public String toString() {
        return "AuthDetails{" +
                "id=" + id +
                ", login='" + login + '\'' +
                ", password='" + password + '\'' +
                ", person=" + person +
                '}';
    }
}
