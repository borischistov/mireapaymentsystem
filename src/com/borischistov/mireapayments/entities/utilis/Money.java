package com.borischistov.mireapayments.entities.utilis;

/**
 * Created with IntelliJ IDEA.
 * User: Boris Chistov
 * Date: 04.11.13
 * Time: 11:19
 * Comment:
 */
public class Money implements Comparable<Money> {

    private long amount;
    private static final int centFactor = 100;

    public Money() {
        this.amount = 0;
    }

    public Money(double amount) {
        this.amount = castDoubleToLong(amount);
    }

    public void add(double m) {
        this.amount = this.amount + castDoubleToLong(m);
    }

    public void add(Money m) {
        this.amount = this.amount + m.amount;
    }

    public void subtract(double m) {
        add(-m);
    }

    public void subtract(Money m) {
        this.amount = this.amount - m.amount;
    }

    private long castDoubleToLong(double d) {
        return (long) (d * centFactor);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Money money = (Money) o;

        if (amount != money.amount) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return (int) (amount ^ (amount >>> 32));
    }

    @Override
    public int compareTo(Money o) {
        long result = amount - o.amount;
        if (result == 0) {
            return 0;
        } else if (result > 0) {
            return 1;
        } else {
            return -1;
        }
    }

    public double getAmount() {
        return amount / 100;
    }


}
