package com.borischistov.mireapayments.entities.utilis;

/**
 * Created with IntelliJ IDEA.
 * User: Boris Chistov
 * Date: 30.11.13
 * Time: 21:36
 * Comment:
 */
public enum PersonType {
    ADMIN, STUDENT
}
