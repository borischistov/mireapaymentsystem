package com.borischistov.mireapayments.entities;

import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: Boris Chistov
 * Date: 04.11.13
 * Time: 13:56
 * Comment:
 */
public class Invoice {

    private int id;
    private String invoiceNumber;
    private Date invoiceCreationDate;
    private PaymentPlan plan;
    private Contract contract;
    private boolean isVerified;

    public Invoice() {
    }

    public Invoice(PaymentPlan plan, Contract contract) {
        this.plan = plan;
        this.contract = contract;
    }

    public Invoice(int id, String invoiceNumber, Date invoiceCreationDate,
                   PaymentPlan plan, Contract contract, boolean verified) {
        this.id = id;
        this.invoiceNumber = invoiceNumber;
        this.invoiceCreationDate = invoiceCreationDate;
        this.plan = plan;
        this.contract = contract;
        isVerified = verified;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getInvoiceNumber() {
        return invoiceNumber;
    }

    public void setInvoiceNumber(String invoiceNumber) {
        this.invoiceNumber = invoiceNumber;
    }

    public Date getInvoiceCreationDate() {
        return invoiceCreationDate;
    }

    public void setInvoiceCreationDate(Date invoiceCreationDate) {
        this.invoiceCreationDate = invoiceCreationDate;
    }

    public PaymentPlan getPlan() {
        return plan;
    }

    public void setPlan(PaymentPlan plan) {
        this.plan = plan;
    }

    public Contract getContract() {
        return contract;
    }

    public void setContract(Contract contract) {
        this.contract = contract;
    }

    public boolean isVerified() {
        return isVerified;
    }

    public void setVerified(boolean verified) {
        isVerified = verified;
    }
}
