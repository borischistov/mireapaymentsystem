package com.borischistov.mireapayments.entities;

import com.borischistov.mireapayments.entities.utilis.PersonType;

/**
 * Created with IntelliJ IDEA.
 * User: Boris Chistov
 * Date: 04.11.13
 * Time: 10:59
 * Comment: Описывает сущность студента
 */
public class Student extends Person{

    private String cardNumber;
    private String recordBookNumber;

    public Student() {
    }

    public Student(int id, String firstName, String lastName, String surname,
                   String cardNumber, String recordBookNumber) {
        super(id, firstName, lastName, surname, PersonType.STUDENT);
        this.cardNumber = cardNumber;
        this.recordBookNumber = recordBookNumber;
    }

    public Student(String firstName, String lastName, String surname,
                   String cardNumber, String recordBookNumber) {
        super(firstName, lastName, surname, PersonType.STUDENT);
        this.cardNumber = cardNumber;
        this.recordBookNumber = recordBookNumber;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getRecordBookNumber() {
        return recordBookNumber;
    }

    public void setRecordBookNumber(String recordBookNumber) {
        this.recordBookNumber = recordBookNumber;
    }
}
