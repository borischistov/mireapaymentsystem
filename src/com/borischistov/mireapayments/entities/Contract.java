package com.borischistov.mireapayments.entities;

/**
 * Created with IntelliJ IDEA.
 * User: Boris Chistov
 * Date: 04.11.13
 * Time: 11:03
 * Comment: Сущность описывает контракт студента.
 */

public class Contract {

    private int id;
    private String contractNumber;
    private StudySemester startDate;
    private StudySemester endDate;

    private Student student;
    private Form form;
    private Speciality speciality;
    private Status status;
    private Group group;

    public String getContractNumber() {
        return contractNumber;
    }

    public void setContractNumber(String contractNumber) {
        this.contractNumber = contractNumber;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public StudySemester getStartDate() {
        return startDate;
    }

    public void setStartDate(StudySemester startDate) {
        this.startDate = startDate;
    }

    public StudySemester getEndDate() {
        return endDate;
    }

    public void setEndDate(StudySemester endDate) {
        this.endDate = endDate;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public Form getForm() {
        return form;
    }

    public void setForm(Form form) {
        this.form = form;
    }

    public Speciality getSpeciality() {
        return speciality;
    }

    public void setSpeciality(Speciality speciality) {
        this.speciality = speciality;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }
}
