package com.borischistov.mireapayments.entities;

/**
 * Created with IntelliJ IDEA.
 * User: Boris Chistov
 * Date: 04.11.13
 * Time: 10:57
 * Comment: Сущность писывает специальность студента
 */
public class Speciality {

    private int id;
    private String name;
    private String specialityCode;

    public Speciality() {
    }

    public Speciality(int id, String name, String specialityCode) {
        this.id = id;
        this.name = name;
        this.specialityCode = specialityCode;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSpecialityCode() {
        return specialityCode;
    }

    public void setSpecialityCode(String specialityCode) {
        this.specialityCode = specialityCode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Speciality that = (Speciality) o;

        if (id != that.id) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        if (specialityCode != null ? !specialityCode.equals(that.specialityCode) : that.specialityCode != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (specialityCode != null ? specialityCode.hashCode() : 0);
        return result;
    }
}
