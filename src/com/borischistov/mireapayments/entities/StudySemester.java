package com.borischistov.mireapayments.entities;

/**
 * Created with IntelliJ IDEA.
 * User: Boris Chistov
 * Date: 04.11.13
 * Time: 10:54
 * Comment: Описывает сущность учебного семестра. (Год - Начало учебного года.)
 */
public class StudySemester implements Comparable<StudySemester> {

    private int startYear;
    private int endYear;
    private byte semester;
    private int semesterID;

    public StudySemester() {
    }

    public int getStartYear() {
        return startYear;
    }

    public void setStartYear(int startYear) {
        this.startYear = startYear;
        setEndYear(startYear + 1);
    }

    public byte getSemester() {
        return semester;
    }

    public void setSemester(byte semester) {
        this.semester = semester;
    }

    public int getEndYear() {
        return endYear;
    }

    private void setEndYear(int endYear) {
        this.endYear = endYear;
    }

    public int getSemesterID() {
        return semesterID;
    }

    public void setSemesterID(int semesterID) {
        this.semesterID = semesterID;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        StudySemester semester1 = (StudySemester) o;

        if (endYear != semester1.endYear) return false;
        if (semester != semester1.semester) return false;
        if (semesterID != semester1.semesterID) return false;
        if (startYear != semester1.startYear) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = startYear;
        result = 31 * result + endYear;
        result = 31 * result + (int) semester;
        result = 31 * result + semesterID;
        return result;
    }

    @Override
    public int compareTo(StudySemester o) {
        if (o.getStartYear() > getStartYear()) {
            return 1;
        } else if (o.getStartYear() < getStartYear()) {
            return -1;
        } else {
            if (o.getSemester() > getSemester()) {
                return 1;
            } else if (o.getSemester() < getSemester()) {
                return -1;
            } else {
                return 0;
            }
        }
    }
}
