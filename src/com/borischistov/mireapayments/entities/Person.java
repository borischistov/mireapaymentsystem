package com.borischistov.mireapayments.entities;

import com.borischistov.mireapayments.entities.utilis.PersonType;

/**
 * Created with IntelliJ IDEA.
 * User: Boris Chistov
 * Date: 30.11.13
 * Time: 21:35
 * Comment:
 */
public class Person {

    private int id;
    private String firstName;
    private String lastName;
    private String surname;
    private PersonType type;

    public Person() {
    }

    public Person(String firstName, String lastName, String surname, PersonType type) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.surname = surname;
        this.type = type;
    }

    public Person(int id, String firstName, String lastName, String surname, PersonType type) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.surname = surname;
        this.type = type;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public PersonType getType() {
        return type;
    }

    public void setType(PersonType type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "Person{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", surname='" + surname + '\'' +
                ", type=" + type +
                '}';
    }
}
