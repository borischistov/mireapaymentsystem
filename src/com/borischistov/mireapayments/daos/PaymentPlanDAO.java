package com.borischistov.mireapayments.daos;

import com.borischistov.mireapayments.daos.api.IPaymentPlanDAO;
import com.borischistov.mireapayments.daos.utils.PaymentPlanRowMapper;
import com.borischistov.mireapayments.entities.PaymentPlan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Boris Chistov
 * Date: 25.11.13
 * Time: 0:54
 * Comment:
 */

@Repository
public class PaymentPlanDAO implements IPaymentPlanDAO {

    private static final String SELECT_ALL = "SELECT payment_plan_id, payment_sum, payment_creation_date,\n" +
            "  \"Form\".form_id, form_name,\n" +
            "  \"Semester\".semester_id, semester, start_year,\n" +
            "  \"Speciality\".speciality_id, speciality_name, speciality_num\n" +
            "FROM \"PaymentPlan\"\n" +
            "  LEFT JOIN \"Form\" ON \"PaymentPlan\".form_id = \"Form\".form_id\n" +
            "  LEFT JOIN \"Semester\" ON \"PaymentPlan\".semester_id = \"Semester\".semester_id\n" +
            "  LEFT JOIN \"Speciality\" ON \"PaymentPlan\".speciality_id = \"Speciality\".speciality_id\n" +
            "ORDER BY start_year, semester";
    private static final String SELECT_BY_ID = "SELECT payment_plan_id, payment_sum, payment_creation_date, \n" +
            "  \"Form\".form_id, form_name, \n" +
            "  \"Semester\".semester_id, semester, start_year, \n" +
            "  \"Speciality\".speciality_id, speciality_name, speciality_num\n" +
            "FROM \"PaymentPlan\"\n" +
            "  LEFT JOIN \"Form\" ON \"PaymentPlan\".form_id = \"Form\".form_id\n" +
            "  LEFT JOIN \"Semester\" ON \"PaymentPlan\".semester_id = \"Semester\".semester_id\n" +
            "  LEFT JOIN \"Speciality\" ON \"PaymentPlan\".speciality_id = \"Speciality\".speciality_id\n" +
            "WHERE payment_plan_id = :payment_plan_id \n" +
            "ORDER BY start_year, semester";

    private static final String INSERT = "INSERT INTO \"PaymentPlan\"(payment_sum, payment_creation_date, form_id, " +
            "semester_id, speciality_id) \n" +
            "  VALUES (:payment_sum, :payment_creation_date, :form_id, " +
            ":semester_id, :speciality_id)";

    private static final String UPDATE = "UPDATE \"PaymentPlan\"\n" +
            "SET\n" +
            "  payment_sum = :payment_sum,\n" +
            "  payment_creation_date = :payment_creation_date,\n" +
            "  form_id = :form_id,\n" +
            "  semester_id = :semester_id,\n" +
            "  speciality_id = :speciality_id\n" +
            "WHERE\n" +
            "  payment_plan_id = :payment_plan_id";

    private static final String DELETE = "DELETE FROM \"PaymentPlan\" WHERE payment_plan_id = :payment_plan_id";

    private NamedParameterJdbcTemplate template;

    @Autowired
    private void setDataSource(DataSource dataSource) {
        template = new NamedParameterJdbcTemplate(dataSource);
    }

    @Override
    public List<PaymentPlan> getPlans() {
        return template.query(SELECT_ALL, new PaymentPlanRowMapper());
    }

    @Override
    public PaymentPlan getPlans(int id) {
        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue("payment_plan_id", id);
        return template.queryForObject(SELECT_BY_ID, parameterSource, new PaymentPlanRowMapper());
    }

    @Override
    public void save(PaymentPlan plan) {
        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue("payment_sum", plan.getPaymentSum().getAmount());
        parameterSource.addValue("payment_creation_date", plan.getPaymentPlanCreationDate());
        parameterSource.addValue("form_id", plan.getForm().getId());
        parameterSource.addValue("semester_id", plan.getStudySemester().getSemesterID());
        parameterSource.addValue("speciality_id", plan.getSpeciality().getId());
        KeyHolder keyHolder = new GeneratedKeyHolder();
        template.update(INSERT, parameterSource, keyHolder);
        plan.setId((Integer) keyHolder.getKeys().get("payment_plan_id"));
    }

    @Override
    public void update(PaymentPlan plan) {
        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue("payment_plan_id", plan.getId());
        parameterSource.addValue("payment_sum", plan.getPaymentSum().getAmount());
        parameterSource.addValue("payment_creation_date", plan.getPaymentPlanCreationDate());
        parameterSource.addValue("form_id", plan.getForm().getId());
        parameterSource.addValue("semester_id", plan.getStudySemester().getSemesterID());
        parameterSource.addValue("speciality_id", plan.getSpeciality().getId());
        template.update(UPDATE, parameterSource);
    }

    @Override
    public void delete(PaymentPlan plan) {
        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue("payment_plan_id", plan.getId());
        template.update(DELETE, parameterSource);
    }
}
