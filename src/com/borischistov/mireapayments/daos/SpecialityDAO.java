package com.borischistov.mireapayments.daos;

import com.borischistov.mireapayments.daos.api.ISpecialityDAO;
import com.borischistov.mireapayments.daos.utils.SpecialityRowMapper;
import com.borischistov.mireapayments.entities.Speciality;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: b_chistov
 * Date: 07.11.13
 * Time: 14:56
 */
@Repository
public class SpecialityDAO implements ISpecialityDAO {

    private static final String INSERT = "INSERT INTO \"Speciality\" (speciality_name, speciality_num) VALUES (:speciality_name, :speciality_num)";
    private static final String UPDATE = "UPDATE \"Speciality\" SET speciality_name =:speciality_name, speciality_num =:speciality_num WHERE speciality_id=:speciality_id;";
    private static final String SELECT_ALL = "SELECT speciality_id, speciality_name, speciality_num FROM \"Speciality\"";
    private static final String SELECT_BY_ID = "SELECT speciality_id, speciality_name, speciality_num FROM \"Speciality\" WHERE speciality_id=:speciality_id;";
    private static final String DELETE = "DELETE FROM \"Speciality\" WHERE speciality_id=:speciality_id";

    private NamedParameterJdbcTemplate template;

    @Autowired
    private void setDataSource(DataSource dataSource) {
        template = new NamedParameterJdbcTemplate(dataSource);
    }

    @Override
    public Speciality getSpecialities(int id) {
        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue("speciality_id", id);
        return template.queryForObject(SELECT_BY_ID, parameterSource, new SpecialityRowMapper());
    }

    @Override
    public List<Speciality> getSpecialities() {
        return template.query(SELECT_ALL, new SpecialityRowMapper());
    }

    @Override
    public void addSpeciality(Speciality speciality) {
        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue("speciality_name", speciality.getName());
        parameterSource.addValue("speciality_num", speciality.getSpecialityCode());
        template.update(INSERT, parameterSource);
    }

    @Override
    public void updateSpeciality(Speciality speciality) {
        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue("speciality_id", speciality.getId());
        parameterSource.addValue("speciality_name", speciality.getName());
        parameterSource.addValue("speciality_num", speciality.getSpecialityCode());
        template.update(UPDATE, parameterSource);
    }

    @Override
    public void deleteSpeciality(Speciality speciality) {
        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue("speciality_id", speciality.getId());
        template.update(DELETE, parameterSource);
    }
}
