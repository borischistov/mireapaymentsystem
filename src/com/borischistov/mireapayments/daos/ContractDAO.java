package com.borischistov.mireapayments.daos;

import com.borischistov.mireapayments.daos.api.IContractDAO;
import com.borischistov.mireapayments.daos.utils.ContractRowMapper;
import com.borischistov.mireapayments.entities.Contract;
import com.borischistov.mireapayments.entities.Person;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: b_chistov
 * Date: 11.11.13
 * Time: 12:17
 */
@Repository
public class ContractDAO implements IContractDAO {

    private static final String INSERT = "INSERT INTO \"Contract\" (student_id, form_id,\n" +
            "                        speciality_id, status_id,\n" +
            "                        group_id, start_semester_id,\n" +
            "                        end_semester_id, contract_number)\n" +
            "  VALUES (:student_id, :form_id,\n" +
            "          :speciality_id, :status_id,\n" +
            "          :group_id, :start_semester_id,\n" +
            "          :end_semester_id, :contract_number)";


    private static final String UPDATE = "UPDATE \"Contract\"\n" +
            "SET\n" +
            "  contract_number=:contract_number,\n" +
            "  status_id=:status_id,\n" +
            "  student_id=:student_id,\n" +
            "  form_id=:form_id,\n" +
            "  speciality_id=:speciality_id,\n" +
            "  group_id=:group_id,\n" +
            "  start_semester_id=:start_semester_id,\n" +
            "  end_semester_id=:end_semester_id\n" +
            "WHERE contract_id = :contract_id";

    private static final String SELECT_ALL = "SELECT \"Contract\".student_id AS id, contract_id, contract_number,\n" +
            "  end_semester_id, start_semester_id, \"Contract\".form_id, \"Contract\".group_id, \"Contract\".speciality_id, \"Contract\".status_id,\n" +
            "  first_name, last_name, surname, stud_card_number, stud_record_book_number, type,\n" +
            "  \"startSemester\".semester AS start_semester, \"startSemester\".start_year AS start_start_year,\n" +
            "  \"endSemester\".semester AS end_semester, \"endSemester\".start_year AS end_start_year,\n" +
            "  form_name, speciality_num, speciality_name, group_name,\n" +
            "  status_name\n" +
            "   FROM \"Contract\"\n" +
            "  LEFT JOIN \"Group\" ON \"Group\".group_id = \"Contract\".group_id \n" +
            "  LEFT JOIN \"Student\" ON \"Student\".id = \"Contract\".student_id\n" +
            "  LEFT JOIN \"Semester\" AS \"startSemester\" ON \"startSemester\".semester_id = \"Contract\".start_semester_id \n" +
            "  LEFT JOIN \"Semester\" AS \"endSemester\" ON \"endSemester\".semester_id = \"Contract\".end_semester_id\n" +
            "  LEFT JOIN \"Form\" ON \"Form\".form_id = \"Contract\".form_id\n" +
            "  LEFT JOIN \"Speciality\" ON \"Speciality\".speciality_id = \"Contract\".speciality_id\n" +
            "  LEFT JOIN \"Status\" ON \"Status\".status_id = \"Contract\".status_id\n";

    private static final String SELECT_BY_STUDENT = "SELECT \"Contract\".student_id AS id, contract_id, contract_number,\n" +
            "  end_semester_id, start_semester_id, \"Contract\".form_id, \"Contract\".group_id, \"Contract\".speciality_id, \"Contract\".status_id,\n" +
            "  first_name, last_name, surname, stud_card_number, stud_record_book_number, type, \n" +
            "  \"startSemester\".semester AS start_semester, \"startSemester\".start_year AS start_start_year,\n" +
            "  \"endSemester\".semester AS end_semester, \"endSemester\".start_year AS end_start_year,\n" +
            "  form_name, speciality_num, speciality_name, group_name,\n" +
            "  status_name\n" +
            "   FROM \"Contract\"\n" +
            "  LEFT JOIN \"Group\" ON \"Group\".group_id = \"Contract\".group_id \n" +
            "  LEFT JOIN \"Student\" ON \"Student\".id = \"Contract\".student_id\n" +
            "  LEFT JOIN \"Semester\" AS \"startSemester\" ON \"startSemester\".semester_id = \"Contract\".start_semester_id \n" +
            "  LEFT JOIN \"Semester\" AS \"endSemester\" ON \"endSemester\".semester_id = \"Contract\".end_semester_id\n" +
            "  LEFT JOIN \"Form\" ON \"Form\".form_id = \"Contract\".form_id\n" +
            "  LEFT JOIN \"Speciality\" ON \"Speciality\".speciality_id = \"Contract\".speciality_id\n" +
            "  LEFT JOIN \"Status\" ON \"Status\".status_id = \"Contract\".status_id\n" +
            "  WHERE \"Contract\".student_id = :student_id";

    private static final String SELECT_BY_ID = "SELECT \"Contract\".student_id AS id, contract_id, contract_number,\n" +
            "  end_semester_id, start_semester_id, \"Contract\".form_id, \"Contract\".group_id, \"Contract\".speciality_id, \"Contract\".status_id,\n" +
            "  first_name, last_name, surname, stud_card_number, stud_record_book_number, type,\n" +
            "  \"startSemester\".semester AS start_semester, \"startSemester\".start_year AS start_start_year,\n" +
            "  \"endSemester\".semester AS end_semester, \"endSemester\".start_year AS end_start_year,\n" +
            "  form_name, speciality_num, speciality_name, group_name,\n" +
            "  status_name\n" +
            "   FROM \"Contract\"\n" +
            "  LEFT JOIN \"Group\" ON \"Group\".group_id = \"Contract\".group_id \n" +
            "  LEFT JOIN \"Student\" ON \"Student\".id = \"Contract\".student_id\n" +
            "  LEFT JOIN \"Semester\" AS \"startSemester\" ON \"startSemester\".semester_id = \"Contract\".start_semester_id \n" +
            "  LEFT JOIN \"Semester\" AS \"endSemester\" ON \"endSemester\".semester_id = \"Contract\".end_semester_id\n" +
            "  LEFT JOIN \"Form\" ON \"Form\".form_id = \"Contract\".form_id\n" +
            "  LEFT JOIN \"Speciality\" ON \"Speciality\".speciality_id = \"Contract\".speciality_id\n" +
            "  LEFT JOIN \"Status\" ON \"Status\".status_id = \"Contract\".status_id\n" +
            "  WHERE \"Contract\".contract_id = :contract_id";

    private NamedParameterJdbcTemplate template;

    @Autowired
    private void setDataSource(DataSource dataSource) {
        template = new NamedParameterJdbcTemplate(dataSource);
    }

    @Override
    public List<Contract> getContracts(Person student) {
        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue("student_id", student.getId());
        return template.query(SELECT_BY_STUDENT, parameterSource, new ContractRowMapper());
    }

    @Override
    public List<Contract> getContracts() {
        return template.query(SELECT_ALL, new ContractRowMapper());
    }

    @Override
    public Contract getContract(int id) {
        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue("contract_id", id);
        return template.queryForObject(SELECT_BY_ID, parameterSource, new ContractRowMapper());
    }

    @Override
    public void add(Contract contract) {
        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue("student_id", contract.getStudent().getId());
        parameterSource.addValue("form_id", contract.getForm().getId());
        parameterSource.addValue("speciality_id", contract.getSpeciality().getId());
        parameterSource.addValue("status_id", contract.getStatus().getId());
        parameterSource.addValue("group_id", contract.getGroup().getId());
        parameterSource.addValue("start_semester_id", contract.getStartDate().getSemesterID());
        parameterSource.addValue("end_semester_id", contract.getEndDate().getSemesterID());
        parameterSource.addValue("contract_number", contract.getContractNumber());
        template.update(INSERT, parameterSource);
    }

    @Override
    public void update(Contract contract) {
        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue("student_id", contract.getStudent().getId());
        parameterSource.addValue("form_id", contract.getForm().getId());
        parameterSource.addValue("speciality_id", contract.getSpeciality().getId());
        parameterSource.addValue("status_id", contract.getStatus().getId());
        parameterSource.addValue("group_id", contract.getGroup().getId());
        parameterSource.addValue("start_semester_id", contract.getStartDate().getSemesterID());
        parameterSource.addValue("end_semester_id", contract.getEndDate().getSemesterID());
        parameterSource.addValue("contract_number", contract.getContractNumber());
        parameterSource.addValue("contract_id", contract.getId());
        template.update(UPDATE, parameterSource);
    }

    @Override
    public void delete(Contract contract) {
    }
}
