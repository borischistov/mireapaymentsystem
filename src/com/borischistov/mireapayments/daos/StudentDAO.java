package com.borischistov.mireapayments.daos;

import com.borischistov.mireapayments.daos.api.IStudentDAO;
import com.borischistov.mireapayments.daos.utils.StudentRowMapper;
import com.borischistov.mireapayments.entities.Student;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Boris Chistov
 * Date: 04.11.13
 * Time: 16:33
 * Comment:
 */
@Repository
public class StudentDAO implements IStudentDAO {

    private static final String ADD =
            "INSERT INTO \"Student\" (last_name, first_name, surname, stud_card_number, stud_record_book_number, type)\n" +
                    "  VALUES (:last_name, :first_name, :surname, :stud_card_number, :stud_record_book_number, :type)";
    private static final String SELECT_ALL = "" +
            "SELECT first_name, last_name, surname, stud_card_number, stud_record_book_number, id, type " +
            "FROM \"Student\"";
    private static final String SELECT_BY_CARD = "" +
            "SELECT first_name, last_name, surname, stud_card_number, stud_record_book_number, id, type " +
            "FROM \"Student\" WHERE stud_card_number=:stud_card_number AND stud_record_book_number=:stud_record_book_number";
    private static final String SELECT_BY_ID = "" +
            "SELECT first_name, last_name, surname, stud_card_number, stud_record_book_number, id, type " +
            "FROM \"Student\" WHERE id=:student_id";
    private static final String UPDATE = "UPDATE \"Student\" SET " +
            "first_name=:first_name, " +
            "last_name=:last_name, " +
            "surname=:surname, " +
            "stud_card_number=:stud_card_number, " +
            "stud_record_book_number=:stud_record_book_number, " +
            "type=:type " +
            "WHERE id=:student_id";

    private NamedParameterJdbcTemplate template;

    @Autowired
    private void setDataSource(DataSource dataSource) {
        template = new NamedParameterJdbcTemplate(dataSource);
    }

    @Override
    public Student getStudents(int id) {
        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue("student_id", id);
        return template.queryForObject(SELECT_BY_ID, parameterSource, new StudentRowMapper());
    }

    @Override
    public Student getStudents(String cardNumber, String recordBookNumber) {
        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue("stud_card_number", cardNumber);
        parameterSource.addValue("stud_record_book_number", recordBookNumber);
        return template.queryForObject(SELECT_BY_CARD, parameterSource, new StudentRowMapper());
    }

    @Override
    public List<Student> getStudents() {
        return template.query(SELECT_ALL, new StudentRowMapper());
    }

    @Override
    public void addStudent(Student student) {
        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue("last_name", student.getLastName());
        parameterSource.addValue("first_name", student.getFirstName());
        parameterSource.addValue("surname", student.getSurname());
        parameterSource.addValue("stud_card_number", student.getCardNumber());
        parameterSource.addValue("stud_record_book_number", student.getRecordBookNumber());
        parameterSource.addValue("type", student.getType().name());
        KeyHolder keyHolder = new GeneratedKeyHolder();
        template.update(ADD, parameterSource, keyHolder);
        student.setId((Integer) keyHolder.getKeys().get("id"));
    }

    @Override
    public void update(Student student) {
        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue("last_name", student.getLastName());
        parameterSource.addValue("first_name", student.getFirstName());
        parameterSource.addValue("surname", student.getSurname());
        parameterSource.addValue("stud_card_number", student.getCardNumber());
        parameterSource.addValue("stud_record_book_number", student.getRecordBookNumber());
        parameterSource.addValue("student_id", student.getId());
        parameterSource.addValue("type", student.getType().name());
        template.update(UPDATE, parameterSource);
    }
}
