package com.borischistov.mireapayments.daos;

import com.borischistov.mireapayments.daos.api.IGroupDAO;
import com.borischistov.mireapayments.daos.utils.GroupRowMapper;
import com.borischistov.mireapayments.entities.Group;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Boris Chistov
 * Date: 07.11.13
 * Time: 21:59
 * Comment:
 */
@Repository
public class GroupDAO implements IGroupDAO {

    private NamedParameterJdbcTemplate template;

    private static final String SELECT_ALL = "SELECT group_id, group_name FROM \"Group\"";
    private static final String SELECT_BY_ID = "SELECT group_id, group_name FROM \"Group\" WHERE group_id=:group_id";
    private static final String ADD = "INSERT INTO \"Group\" (group_name) VALUES (:group_name)";
    private static final String UPDATE = "UPDATE \"Group\" SET group_name=:group_name WHERE group_id=:group_id";
    private static final String DELETE = "DELETE FROM \"Group\" WHERE group_id=:group_id";

    @Autowired
    private void setDataSource(DataSource dataSource) {
        template = new NamedParameterJdbcTemplate(dataSource);
    }

    @Override
    public Group getGroups(int id) {
        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue("group_id", id);
        return template.queryForObject(SELECT_BY_ID, parameterSource, new GroupRowMapper());
    }

    @Override
    public List<Group> getGroups() {
        return template.query(SELECT_ALL, new GroupRowMapper());
    }

    @Override
    public void add(Group group) {
        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue("group_name", group.getName());
        template.update(ADD, parameterSource);
    }

    @Override
    public void update(Group group) {
        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue("group_id", group.getId());
        parameterSource.addValue("group_name", group.getName());
        template.update(UPDATE, parameterSource);
    }

    @Override
    public void delete(Group group) {
        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue("group_id", group.getId());
        template.update(DELETE, parameterSource);
    }
}
