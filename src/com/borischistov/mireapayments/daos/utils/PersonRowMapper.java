package com.borischistov.mireapayments.daos.utils;

import com.borischistov.mireapayments.entities.Person;
import com.borischistov.mireapayments.entities.utilis.PersonType;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created with IntelliJ IDEA.
 * User: Boris Chistov
 * Date: 01.12.13
 * Time: 22:41
 * Comment:
 */
public class PersonRowMapper implements RowMapper<Person> {

    @Override
    public Person mapRow(ResultSet resultSet, int i) throws SQLException {
        return new Person(
                resultSet.getInt("id"),
                resultSet.getString("first_name"),
                resultSet.getString("last_name"),
                resultSet.getString("surname"),
                PersonType.valueOf(resultSet.getString("type"))
        );
    }
}
