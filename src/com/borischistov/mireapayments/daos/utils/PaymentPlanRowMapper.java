package com.borischistov.mireapayments.daos.utils;

import com.borischistov.mireapayments.entities.PaymentPlan;
import com.borischistov.mireapayments.entities.utilis.Money;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created with IntelliJ IDEA.
 * User: Boris Chistov
 * Date: 25.11.13
 * Time: 1:15
 * Comment:
 */
public class PaymentPlanRowMapper implements RowMapper<PaymentPlan> {

    @Override
    public PaymentPlan mapRow(ResultSet resultSet, int i) throws SQLException {
        PaymentPlan plan = new PaymentPlan();
        plan.setId(resultSet.getInt("payment_plan_id"));
        plan.setPaymentSum(new Money(resultSet.getDouble("payment_sum")));
        plan.setPaymentPlanCreationDate(resultSet.getDate("payment_creation_date"));
        plan.setForm(new FormRowMapper().mapRow(resultSet, i));
        plan.setStudySemester(new StudySemesterRowMapper().mapRow(resultSet, i));
        plan.setSpeciality(new SpecialityRowMapper().mapRow(resultSet, i));
        return plan;
    }
}
