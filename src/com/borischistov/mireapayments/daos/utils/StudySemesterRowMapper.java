package com.borischistov.mireapayments.daos.utils;

import com.borischistov.mireapayments.entities.StudySemester;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created with IntelliJ IDEA.
 * User: b_chistov
 * Date: 08.11.13
 * Time: 15:12
 */
public class StudySemesterRowMapper implements RowMapper<StudySemester> {

    @Override
    public StudySemester mapRow(ResultSet resultSet, int i) throws SQLException {
        StudySemester semester = new StudySemester();
        semester.setSemesterID(resultSet.getInt("semester_id"));
        semester.setSemester(resultSet.getByte("semester"));
        semester.setStartYear(resultSet.getInt("start_year"));
        return semester;
    }
}
