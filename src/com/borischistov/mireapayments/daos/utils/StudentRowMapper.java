package com.borischistov.mireapayments.daos.utils;

import com.borischistov.mireapayments.entities.Student;
import com.borischistov.mireapayments.entities.utilis.PersonType;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created with IntelliJ IDEA.
 * User: b_chistov
 * Date: 05.11.13
 * Time: 11:37
 */
public class StudentRowMapper implements RowMapper<Student> {

    @Override
    public Student mapRow(ResultSet resultSet, int i) throws SQLException {
        Student student = new Student();
        student.setFirstName(resultSet.getString("first_name"));
        student.setLastName(resultSet.getString("last_name"));
        student.setId(resultSet.getInt("id"));
        student.setSurname(resultSet.getString("surname"));
        student.setCardNumber(resultSet.getString("stud_card_number"));
        student.setRecordBookNumber(resultSet.getString("stud_record_book_number"));
        student.setType(PersonType.valueOf(resultSet.getString("type")));
        return student;
    }
}
