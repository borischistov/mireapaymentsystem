package com.borischistov.mireapayments.daos.utils;

import com.borischistov.mireapayments.entities.Speciality;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created with IntelliJ IDEA.
 * User: b_chistov
 * Date: 07.11.13
 * Time: 15:08
 */
public class SpecialityRowMapper implements RowMapper<Speciality> {
    @Override
    public Speciality mapRow(ResultSet resultSet, int i) throws SQLException {
        Speciality speciality = new Speciality();
        speciality.setId(resultSet.getInt("speciality_id"));
        speciality.setName(resultSet.getString("speciality_name"));
        speciality.setSpecialityCode(resultSet.getString("speciality_num"));
        return speciality;
    }
}
