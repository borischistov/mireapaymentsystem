package com.borischistov.mireapayments.daos.utils;

import com.borischistov.mireapayments.entities.Contract;
import com.borischistov.mireapayments.entities.StudySemester;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created with IntelliJ IDEA.
 * User: b_chistov
 * Date: 11.11.13
 * Time: 19:49
 */
public class ContractRowMapper implements RowMapper<Contract> {

    @Override
    public Contract mapRow(ResultSet resultSet, int i) throws SQLException {
        Contract contract = new Contract();
        contract.setId(resultSet.getInt("contract_id"));
        contract.setStudent(new StudentRowMapper().mapRow(resultSet, i));
        contract.setForm(new FormRowMapper().mapRow(resultSet, i));
        contract.setContractNumber(resultSet.getString("contract_number"));
        contract.setGroup(new GroupRowMapper().mapRow(resultSet, i));
        contract.setSpeciality(new SpecialityRowMapper().mapRow(resultSet, i));
        contract.setStatus(new StatusRowMapper().mapRow(resultSet, i));
        StudySemester semester = new StudySemester();
        semester.setSemesterID(resultSet.getInt("start_semester_id"));
        semester.setSemester(resultSet.getByte("start_semester"));
        semester.setStartYear(resultSet.getInt("start_start_year"));
        contract.setStartDate(semester);
        semester = new StudySemester();
        semester.setSemesterID(resultSet.getInt("end_semester_id"));
        semester.setSemester(resultSet.getByte("end_semester"));
        semester.setStartYear(resultSet.getInt("end_start_year"));
        contract.setEndDate(semester);
        return contract;
    }
}
