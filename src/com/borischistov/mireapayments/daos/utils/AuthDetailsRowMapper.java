package com.borischistov.mireapayments.daos.utils;

import com.borischistov.mireapayments.entities.utilis.AuthDetails;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created with IntelliJ IDEA.
 * User: b_chistov
 * Date: 28.11.13
 * Time: 14:47
 */
public class AuthDetailsRowMapper implements RowMapper<AuthDetails> {

    @Override
    public AuthDetails mapRow(ResultSet resultSet, int i) throws SQLException {
        AuthDetails result = new AuthDetails(
                resultSet.getInt("auth_id"),
                resultSet.getString("login"),
                resultSet.getString("password"),
                resultSet.getString("authorities"),
                new PersonRowMapper().mapRow(resultSet, i)
        );
        return result;
    }
}
