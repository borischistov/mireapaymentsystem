package com.borischistov.mireapayments.daos.utils;

import com.borischistov.mireapayments.entities.Contract;
import com.borischistov.mireapayments.entities.Invoice;
import com.borischistov.mireapayments.entities.PaymentPlan;
import com.borischistov.mireapayments.entities.utilis.Money;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created with IntelliJ IDEA.
 * User: b_chistov
 * Date: 06.12.13
 * Time: 19:44
 */
public class InvoiceRowMapper implements RowMapper<Invoice> {

    private Contract contract = null;
    private InvoiceRowMapperStates state;

    public enum InvoiceRowMapperStates {
        BY_CONTRACT
    }

    public InvoiceRowMapper(Contract c, InvoiceRowMapperStates state) {
        this.contract = c;
        this.state = state;
    }

    @Override
    public Invoice mapRow(ResultSet resultSet, int i) throws SQLException {
        if(state.equals(InvoiceRowMapperStates.BY_CONTRACT) && contract != null){
            return mapByContract(resultSet, i);
        }else {
            return null;
        }
    }

   private Invoice mapByContract(ResultSet resultSet, int i) throws SQLException {
       PaymentPlan plan = new PaymentPlan();
       plan.setSpeciality(contract.getSpeciality());
       plan.setForm(contract.getForm());
       double sum = resultSet.getDouble("payment_sum");
       if(!resultSet.wasNull()){
        plan.setPaymentSum(new Money(sum));
       }
       plan.setPaymentPlanCreationDate(resultSet.getDate("payment_creation_date"));
       plan.setId(resultSet.getInt("payment_plan_id"));
       plan.setStudySemester(new StudySemesterRowMapper().mapRow(resultSet, i));
       Invoice result = new Invoice();

       result.setPlan(plan);
       result.setContract(contract);

       result.setId(resultSet.getInt("invoice_id"));
       result.setInvoiceNumber(resultSet.getString("invoice_number"));
       result.setInvoiceCreationDate(resultSet.getDate("invoice_creation_date"));
       result.setVerified(resultSet.getBoolean("is_verified"));

       return result;
   }
}
