package com.borischistov.mireapayments.daos.utils;

import com.borischistov.mireapayments.entities.Form;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created with IntelliJ IDEA.
 * User: b_chistov
 * Date: 06.11.13
 * Time: 16:25
 */
public class FormRowMapper implements RowMapper<Form> {

    @Override
    public Form mapRow(ResultSet resultSet, int i) throws SQLException {
        return new Form(resultSet.getInt("form_id"), resultSet.getString("form_name"));
    }
}
