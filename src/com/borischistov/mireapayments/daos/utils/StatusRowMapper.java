package com.borischistov.mireapayments.daos.utils;

import com.borischistov.mireapayments.entities.Status;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created with IntelliJ IDEA.
 * User: b_chistov
 * Date: 07.11.13
 * Time: 16:20
 */
public class StatusRowMapper implements RowMapper<Status> {

    @Override
    public Status mapRow(ResultSet resultSet, int i) throws SQLException {
        return new Status(resultSet.getInt("status_id"), resultSet.getString("status_name"));
    }
}
