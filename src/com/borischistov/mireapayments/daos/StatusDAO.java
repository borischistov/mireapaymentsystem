package com.borischistov.mireapayments.daos;

import com.borischistov.mireapayments.daos.api.IStatusDAO;
import com.borischistov.mireapayments.daos.utils.StatusRowMapper;
import com.borischistov.mireapayments.entities.Status;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: b_chistov
 * Date: 07.11.13
 * Time: 16:13
 */
@Repository
public class StatusDAO implements IStatusDAO {

    private static final String SELECT_ALL = "SELECT status_id, status_name FROM \"Status\"";
    private static final String SELECT_BY_ID = "SELECT status_id, status_name FROM \"Status\" WHERE status_id =:status_id";
    private static final String ADD = "INSERT INTO \"Status\" (status_name) VALUES (:status_name)";
    private static final String UPDATE = "UPDATE \"Status\" SET status_name=:status_name WHERE status_id=:status_id";
    private static final String DELETE = "DELETE FROM \"Status\" WHERE status_id=:status_id";

    private NamedParameterJdbcTemplate template;

    @Autowired
    private void setDataSource(DataSource dataSource) {
        template = new NamedParameterJdbcTemplate(dataSource);
    }

    @Override
    public Status getStatuses(int id) {
        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue("status_id", id);
        return template.queryForObject(SELECT_BY_ID, parameterSource, new StatusRowMapper());
    }

    @Override
    public List<Status> getStatuses() {
        return template.query(SELECT_ALL, new StatusRowMapper());
    }

    @Override
    public void add(Status status) {
        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue("status_name", status.getName());
        template.update(ADD, parameterSource);
    }

    @Override
    public void update(Status status) {
        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue("status_id", status.getId());
        parameterSource.addValue("status_name", status.getName());
        template.update(UPDATE, parameterSource);
    }

    @Override
    public void delete(Status status) {
        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue("status_id", status.getId());
        template.update(DELETE, parameterSource);
    }
}
