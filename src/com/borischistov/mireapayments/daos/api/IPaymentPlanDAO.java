package com.borischistov.mireapayments.daos.api;

import com.borischistov.mireapayments.entities.PaymentPlan;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Boris Chistov
 * Date: 25.11.13
 * Time: 0:52
 * Comment:
 */
public interface IPaymentPlanDAO {

    public List<PaymentPlan> getPlans();

    public PaymentPlan getPlans(int id);

    public void save(PaymentPlan plan);

    public void update(PaymentPlan plan);

    public void delete(PaymentPlan plan);
}
