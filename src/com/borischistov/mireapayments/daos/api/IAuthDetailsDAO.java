package com.borischistov.mireapayments.daos.api;

import com.borischistov.mireapayments.entities.utilis.AuthDetails;

/**
 * Created with IntelliJ IDEA.
 * User: b_chistov
 * Date: 28.11.13
 * Time: 13:19
 */
public interface IAuthDetailsDAO {

    public void addAuthDetails(AuthDetails details);

    public void updateAuthDetails(AuthDetails details);

    public AuthDetails getAuthDetails(String login, String password);

    public AuthDetails getAuthDetails(String login);
}
