package com.borischistov.mireapayments.daos.api;

import com.borischistov.mireapayments.entities.Student;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Boris Chistov
 * Date: 04.11.13
 * Time: 16:38
 * Comment:
 */
public interface IStudentDAO {

    public Student getStudents(int id);

    public Student getStudents(String cardNumber, String recordBookNumber);

    public List<Student> getStudents();

    public void addStudent(Student student);

    public void update(Student student);
}
