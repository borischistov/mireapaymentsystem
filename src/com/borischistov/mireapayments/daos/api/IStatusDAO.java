package com.borischistov.mireapayments.daos.api;

import com.borischistov.mireapayments.entities.Status;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: b_chistov
 * Date: 07.11.13
 * Time: 16:11
 */
public interface IStatusDAO {

    public Status getStatuses(int id);

    public List<Status> getStatuses();

    public void add(Status status);

    public void update(Status status);

    public void delete(Status status);
}
