package com.borischistov.mireapayments.daos.api;

import com.borischistov.mireapayments.entities.StudySemester;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: b_chistov
 * Date: 08.11.13
 * Time: 11:47
 */
public interface ISemesterDAO {

    public List<StudySemester> getSemesters();

    public StudySemester getSemesters(int id);

    public List<StudySemester> getSemesters(int startYear, int endYear);

    public List<StudySemester> getSemesters(StudySemester startSemester, StudySemester endSemester);

    public void add(StudySemester semester);

    public void update(StudySemester semester);

    public void delete(StudySemester semester);
}
