package com.borischistov.mireapayments.daos.api;

import com.borischistov.mireapayments.entities.Contract;
import com.borischistov.mireapayments.entities.Person;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: b_chistov
 * Date: 11.11.13
 * Time: 12:06
 */
public interface IContractDAO {

    public List<Contract> getContracts(Person student);

    public List<Contract> getContracts();

    public Contract getContract(int id);

    public void add(Contract contract);

    public void update(Contract contract);

    public void delete(Contract contract);
}
