package com.borischistov.mireapayments.daos.api;

import com.borischistov.mireapayments.entities.Group;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Boris Chistov
 * Date: 07.11.13
 * Time: 21:53
 * Comment:
 */
public interface IGroupDAO {

    public Group getGroups(int id);

    public List<Group> getGroups();

    public void add(Group group);

    public void update(Group group);

    public void delete(Group group);
}
