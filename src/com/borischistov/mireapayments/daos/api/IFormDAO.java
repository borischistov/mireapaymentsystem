package com.borischistov.mireapayments.daos.api;

import com.borischistov.mireapayments.entities.Form;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: b_chistov
 * Date: 06.11.13
 * Time: 15:16
 */
public interface IFormDAO {

    public Form getForms(int id);

    public List<Form> getForms();

    public void addForm(Form form);

    public void update(Form form);

    public void delete(Form form);
}
