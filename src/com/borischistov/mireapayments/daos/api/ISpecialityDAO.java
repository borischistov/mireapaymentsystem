package com.borischistov.mireapayments.daos.api;

import com.borischistov.mireapayments.entities.Speciality;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: b_chistov
 * Date: 07.11.13
 * Time: 14:35
 */
public interface ISpecialityDAO {

    public Speciality getSpecialities(int id);

    public List<Speciality> getSpecialities();

    public void addSpeciality(Speciality speciality);

    public void updateSpeciality(Speciality speciality);

    public void deleteSpeciality(Speciality speciality);
}
