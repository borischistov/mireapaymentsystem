package com.borischistov.mireapayments.daos.api;

import com.borischistov.mireapayments.entities.Contract;
import com.borischistov.mireapayments.entities.Invoice;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: b_chistov
 * Date: 04.12.13
 * Time: 15:09
 */
public interface IInvoiceDAO {

    public void add(Invoice invoice);

    public void update(Invoice invoice);

    public Invoice getInvoices(int id);

    public List<Invoice> getInvoices(Contract contract);

}
