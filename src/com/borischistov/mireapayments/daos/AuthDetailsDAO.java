package com.borischistov.mireapayments.daos;

import com.borischistov.mireapayments.daos.api.IAuthDetailsDAO;
import com.borischistov.mireapayments.daos.utils.AuthDetailsRowMapper;
import com.borischistov.mireapayments.entities.utilis.AuthDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;

/**
 * Created with IntelliJ IDEA.
 * User: b_chistov
 * Date: 28.11.13
 * Time: 13:19
 */
@Repository
public class AuthDetailsDAO implements IAuthDetailsDAO {

    private static final String SELECT = "SELECT\n" +
            "  \"AuthDetails\".id AS auth_id,\n" +
            "  login,\n" +
            "  password,\n" +
            "  authorities,\n" +
            "  person_id AS id,\n" +
            "  first_name,\n" +
            "  last_name,\n" +
            "  surname,\n" +
            "  type\n" +
            "FROM \"AuthDetails\"\n" +
            "  LEFT JOIN \"Person\" ON person_id = \"Person\".id\n" +
            "WHERE login = :login AND password = :password";
    private static final String SELECT_BY_LOGIN = "SELECT\n" +
            "  \"AuthDetails\".id AS auth_id,\n" +
            "  login,\n" +
            "  password,\n" +
            "  authorities,\n" +
            "  person_id AS id,\n" +
            "  first_name,\n" +
            "  last_name,\n" +
            "  surname,\n" +
            "  type\n" +
            "FROM \"AuthDetails\"\n" +
            "  LEFT JOIN \"Person\" ON person_id = \"Person\".id\n" +
            "WHERE login = :login";
    private static final String ADD = "INSERT INTO \"AuthDetails\" (login, password, authorities, person_id) \n" +
            "  VALUES (:login, :password, :authorities, :person_id)";
    private static final String UPDATE = "UPDATE \"AuthDetails\"\n" +
            "SET login = :login, password = :password, authorities =:authorities\n" +
            "WHERE person_id = :person_id ";

    private NamedParameterJdbcTemplate template;

    @Autowired
    private void setDataSource(DataSource dataSource) {
        template = new NamedParameterJdbcTemplate(dataSource);
    }

    @Override
    public void addAuthDetails(AuthDetails details) {
        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue("login", details.getUsername());
        parameterSource.addValue("password", details.getPassword());
        parameterSource.addValue("authorities", details.getAuthoritiesAsString());
        parameterSource.addValue("person_id", details.getPerson().getId());
        GeneratedKeyHolder keyHolder = new GeneratedKeyHolder();
        template.update(ADD, parameterSource, keyHolder);
        details.setId((Integer) keyHolder.getKeys().get("id"));
    }

    @Override
    public void updateAuthDetails(AuthDetails details) {
        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue("login", details.getUsername());
        parameterSource.addValue("password", details.getPassword());
        parameterSource.addValue("authorities", details.getAuthoritiesAsString());
        parameterSource.addValue("person_id", details.getPerson().getId());
        template.update(UPDATE, parameterSource);
    }

    @Override
    public AuthDetails getAuthDetails(String login, String password) {
        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue("login", login);
        parameterSource.addValue("password", password);
        return template.queryForObject(SELECT, parameterSource, new AuthDetailsRowMapper());
    }

    @Override
    public AuthDetails getAuthDetails(String login) {
        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue("login", login);
        return template.queryForObject(SELECT_BY_LOGIN, parameterSource, new AuthDetailsRowMapper());
    }
}
