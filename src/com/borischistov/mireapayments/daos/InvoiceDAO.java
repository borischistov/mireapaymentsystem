package com.borischistov.mireapayments.daos;

import com.borischistov.mireapayments.daos.api.IInvoiceDAO;
import com.borischistov.mireapayments.daos.utils.InvoiceRowMapper;
import com.borischistov.mireapayments.entities.Contract;
import com.borischistov.mireapayments.entities.Invoice;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: b_chistov
 * Date: 04.12.13
 * Time: 15:38
 */
@Repository
public class InvoiceDAO implements IInvoiceDAO {

    private NamedParameterJdbcTemplate template;

    private static final String INSERT = "INSERT INTO \"Invoice\" (invoice_creation_date, contract_id,\n" +
            "                       payment_plan_id, invoice_number, is_verified)\n" +
            "    values (:invoice_creation_date, :contract_id,\n" +
            "            :payment_plan_id, :invoice_number, :is_verified)";

    private static final String SELECT_INVOICES_BY_CONTRACT_WITH_NULLS = "(SELECT\n" +
            "   \"Semester\".semester_id,\n" +
            "   \"Semester\".semester,\n" +
            "   \"Semester\".start_year,\n" +
            "   \"PaymentPlan\".payment_sum,\n" +
            "   \"PaymentPlan\".payment_plan_id,\n" +
            "   \"PaymentPlan\".payment_creation_date,\n" +
            "   \"Contract\".contract_number,\n" +
            "   \"Invoice\".*\n" +
            " FROM \"Semester\"\n" +
            "   LEFT JOIN \"Contract\" ON \"Contract\".contract_id = :contract\n" +
            "   LEFT JOIN \"PaymentPlan\" ON \"PaymentPlan\".semester_id = \"Semester\".semester_id\n" +
            "                              AND \"PaymentPlan\".speciality_id = \"Contract\".speciality_id\n" +
            "                              AND \"PaymentPlan\".form_id = \"Contract\".form_id\n" +
            "   LEFT JOIN \"Invoice\" ON \"Invoice\".payment_plan_id = \"PaymentPlan\".payment_plan_id\n" +
            "                          AND \"Invoice\".contract_id = \"Contract\".contract_id\n" +
            " WHERE start_year > :start_year AND start_year < :end_year\n" +
            " UNION\n" +
            " SELECT\n" +
            "   \"Semester\".semester_id,\n" +
            "   \"Semester\".semester,\n" +
            "   \"Semester\".start_year,\n" +
            "   \"PaymentPlan\".payment_sum,\n" +
            "   \"PaymentPlan\".payment_plan_id,\n" +
            "   \"PaymentPlan\".payment_creation_date,\n" +
            "   \"Contract\".contract_number,\n" +
            "   \"Invoice\".*\n" +
            " FROM \"Semester\"\n" +
            "   LEFT JOIN \"Contract\" ON \"Contract\".contract_id = :contract\n" +
            "   LEFT JOIN \"PaymentPlan\" ON \"PaymentPlan\".semester_id = \"Semester\".semester_id\n" +
            "                              AND \"PaymentPlan\".speciality_id = \"Contract\".speciality_id\n" +
            "                              AND \"PaymentPlan\".form_id = \"Contract\".form_id\n" +
            "   LEFT JOIN \"Invoice\" ON \"Invoice\".payment_plan_id = \"PaymentPlan\".payment_plan_id\n" +
            "                          AND \"Invoice\".contract_id = \"Contract\".contract_id\n" +
            " WHERE start_year = :start_year AND semester >= :start_semester\n" +
            " UNION\n" +
            " SELECT\n" +
            "   \"Semester\".semester_id,\n" +
            "   \"Semester\".semester,\n" +
            "   \"Semester\".start_year,\n" +
            "   \"PaymentPlan\".payment_sum,\n" +
            "   \"PaymentPlan\".payment_plan_id,\n" +
            "   \"PaymentPlan\".payment_creation_date,\n" +
            "   \"Contract\".contract_number,\n" +
            "   \"Invoice\".*\n" +
            " FROM \"Semester\"\n" +
            "   LEFT JOIN \"Contract\" ON \"Contract\".contract_id = :contract\n" +
            "   LEFT JOIN \"PaymentPlan\" ON \"PaymentPlan\".semester_id = \"Semester\".semester_id\n" +
            "                              AND \"PaymentPlan\".speciality_id = \"Contract\".speciality_id\n" +
            "                              AND \"PaymentPlan\".form_id = \"Contract\".form_id\n" +
            "   LEFT JOIN \"Invoice\" ON \"Invoice\".payment_plan_id = \"PaymentPlan\".payment_plan_id\n" +
            "                          AND \"Invoice\".contract_id = \"Contract\".contract_id\n" +
            " WHERE start_year = :end_year AND semester <= :end_semester)";

    @Autowired
    private void setDataSource(DataSource dataSource) {
        template = new NamedParameterJdbcTemplate(dataSource);
    }

    @Override
    public void add(Invoice invoice) {
        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue("contract_id", invoice.getContract().getId());
        parameterSource.addValue("invoice_creation_date", invoice.getInvoiceCreationDate());
        parameterSource.addValue("invoice_number", invoice.getInvoiceNumber());
        parameterSource.addValue("is_verified", invoice.isVerified());
        parameterSource.addValue("payment_plan_id", invoice.getPlan().getId());
        GeneratedKeyHolder keyHolder = new GeneratedKeyHolder();
        template.update(INSERT, parameterSource, keyHolder);
        invoice.setId((Integer) keyHolder.getKeys().get("invoice_id"));
    }

    @Override
    public void update(Invoice invoice) {
    }

    @Override
    public Invoice getInvoices(int id) {
        return null;
    }

    @Override
    public List<Invoice> getInvoices(Contract contract) {
        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue("contract", contract.getId());
        parameterSource.addValue("end_semester", contract.getEndDate().getSemester());
        parameterSource.addValue("end_year", contract.getEndDate().getStartYear());
        parameterSource.addValue("start_semester", contract.getStartDate().getSemester());
        parameterSource.addValue("start_year", contract.getStartDate().getStartYear());
        return template.query(SELECT_INVOICES_BY_CONTRACT_WITH_NULLS,
                parameterSource,
                new InvoiceRowMapper(contract, InvoiceRowMapper.InvoiceRowMapperStates.BY_CONTRACT));
    }
}
