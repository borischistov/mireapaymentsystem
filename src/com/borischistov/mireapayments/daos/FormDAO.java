package com.borischistov.mireapayments.daos;

import com.borischistov.mireapayments.daos.api.IFormDAO;
import com.borischistov.mireapayments.daos.utils.FormRowMapper;
import com.borischistov.mireapayments.entities.Form;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: b_chistov
 * Date: 06.11.13
 * Time: 15:17
 */
@Repository
public class FormDAO implements IFormDAO {


    private NamedParameterJdbcTemplate template;

    private static final String ADD = "INSERT INTO \"Form\"(form_name) VALUES (:form_name);";
    private static final String SELECT_ALL = "SELECT form_id, form_name FROM \"Form\"";
    private static final String SELECT_BY_ID = "SELECT form_id, form_name FROM \"Form\" WHERE form_id=:form_id";
    private static final String UPDATE = "UPDATE \"Form\" SET form_name=:form_name WHERE form_id=:form_id";
    private static final String DELETE = "DELETE FROM \"Form\" WHERE form_id=:form_id";

    @Autowired
    private void setDataSource(DataSource dataSource) {
        template = new NamedParameterJdbcTemplate(dataSource);
    }

    @Override
    public Form getForms(int id) {
        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue("form_id", id);
        return template.queryForObject(SELECT_BY_ID, parameterSource, new FormRowMapper());
    }

    @Override
    public List<Form> getForms() {
        return template.query(SELECT_ALL, new FormRowMapper());
    }

    @Override
    public void addForm(Form form) {
        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue("form_name", form.getName());
        template.update(ADD, parameterSource);
    }

    @Override
    public void update(Form form) {
        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue("form_name", form.getName());
        parameterSource.addValue("form_id", form.getId());
        template.update(UPDATE, parameterSource);
    }

    @Override
    public void delete(Form form) {
        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue("form_id", form.getId());
        template.update(DELETE, parameterSource);
    }
}
