package com.borischistov.mireapayments.daos;

import com.borischistov.mireapayments.daos.api.ISemesterDAO;
import com.borischistov.mireapayments.daos.utils.StudySemesterRowMapper;
import com.borischistov.mireapayments.entities.StudySemester;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: b_chistov
 * Date: 08.11.13
 * Time: 14:12
 */
@Repository
public class SemesterDAO implements ISemesterDAO {

    private static final String SELECT_BY_ID = "SELECT semester_id, semester, start_year " +
            "FROM \"Semester\" WHERE semester_id=:semester_id";
    private static final String SELECT_ALL = "SELECT semester_id, semester, start_year FROM \"Semester\" ORDER BY start_year";
    private static final String SELECT_IN_RANGE = "SELECT semester_id, semester, start_year FROM \"Semester\" " +
            "WHERE start_year>=:start_year AND start_year<=end_year  ORDER BY start_year";
    private static final String SELECT_BETWEEN_SEMESTERS = "SELECT\n" +
            "  semester_id,\n" +
            "  semester,\n" +
            "  start_year\n" +
            "FROM \"Semester\"\n" +
            "WHERE start_year > :start_year AND start_year < :end_year\n" +
            "UNION\n" +
            "SELECT\n" +
            "  semester_id,\n" +
            "  semester,\n" +
            "  start_year\n" +
            "FROM \"Semester\" WHERE start_year = :start_year AND semester >= :start_semester\n" +
            "UNION\n" +
            "SELECT\n" +
            "  semester_id,\n" +
            "  semester,\n" +
            "  start_year\n" +
            "FROM \"Semester\" WHERE start_year = :end_year AND semester >= :end_semester\n" +
            "  \n";
    private static final String ADD = "INSERT INTO \"Semester\" (semester, start_year) " +
            "VALUES (:semester, :start_year)";
    private static final String UPDATE = "UPDATE \"Semester\" " +
            "SET semester=:semester, start_year=:start_year " +
            "WHERE semester_id=:semester_id";
    private static final String DELETE = "DELETE FROM \"Semester\" WHERE semester_id=:semester_id";


    private NamedParameterJdbcTemplate template;

    @Autowired
    private void setDataSource(DataSource dataSource) {
        template = new NamedParameterJdbcTemplate(dataSource);
    }

    @Override
    public List<StudySemester> getSemesters() {
        return template.query(SELECT_ALL, new StudySemesterRowMapper());
    }

    @Override
    public StudySemester getSemesters(int id) {
        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue("semester_id", id);
        return template.queryForObject(SELECT_BY_ID, parameterSource, new StudySemesterRowMapper());
    }

    @Override
    public List<StudySemester> getSemesters(int startYear, int endYear) {
        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue("start_year", startYear);
        parameterSource.addValue("end_year", endYear);
        return template.query(SELECT_IN_RANGE, parameterSource, new StudySemesterRowMapper());
    }

    @Override
    public void add(StudySemester semester) {
        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue("semester", semester.getSemester());
        parameterSource.addValue("start_year", semester.getStartYear());
        KeyHolder keyHolder = new GeneratedKeyHolder();
        template.update(ADD, parameterSource, keyHolder);
        semester.setSemesterID((Integer) keyHolder.getKeys().get("semester_id"));
    }

    @Override
    public void update(StudySemester semester) {
        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue("semester_id", semester.getSemesterID());
        parameterSource.addValue("semester", semester.getSemester());
        parameterSource.addValue("start_year", semester.getStartYear());
        template.update(UPDATE, parameterSource);
    }

    @Override
    public void delete(StudySemester semester) {
        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue("semester_id", semester.getSemesterID());
        template.update(DELETE, parameterSource);
    }

    @Override
    public List<StudySemester> getSemesters(StudySemester startSemester, StudySemester endSemester) {
        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue("end_semester", endSemester.getSemester());
        parameterSource.addValue("end_year", endSemester.getStartYear());
        parameterSource.addValue("start_semester", startSemester.getSemester());
        parameterSource.addValue("start_year", startSemester.getStartYear());
        return template.query(SELECT_BETWEEN_SEMESTERS, parameterSource, new StudySemesterRowMapper());
    }
}
