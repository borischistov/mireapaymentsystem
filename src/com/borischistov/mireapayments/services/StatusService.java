package com.borischistov.mireapayments.services;

import com.borischistov.mireapayments.daos.api.IStatusDAO;
import com.borischistov.mireapayments.entities.Status;
import com.borischistov.mireapayments.services.api.IStatusService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: b_chistov
 * Date: 07.11.13
 * Time: 16:28
 */
@Service
public class StatusService implements IStatusService {

    @Autowired
    private IStatusDAO statusDAO;


    @Override
    public Status getStatuses(int id) {
        return statusDAO.getStatuses(id);
    }

    @Override
    public List<Status> getStatuses() {
        return statusDAO.getStatuses();
    }

    @Override
    public void save(Status status) {
        if (status.getId() > 0) {
            statusDAO.update(status);
        } else if (status.getId() == 0) {
            statusDAO.add(status);
        }
    }

    @Override
    public void delete(Status status) {
        statusDAO.delete(status);
    }
}
