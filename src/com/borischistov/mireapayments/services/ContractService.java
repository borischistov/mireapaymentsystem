package com.borischistov.mireapayments.services;

import com.borischistov.mireapayments.daos.api.IContractDAO;
import com.borischistov.mireapayments.entities.Contract;
import com.borischistov.mireapayments.entities.Person;
import com.borischistov.mireapayments.services.api.IContractService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: b_chistov
 * Date: 11.11.13
 * Time: 17:36
 */
@Service
public class ContractService implements IContractService {

    @Autowired
    private IContractDAO contractDAO;

    @Override
    public void save(Contract contract) {
        if (contract.getId() > 0) {
            contractDAO.update(contract);
        } else if (contract.getId() == 0) {
            contractDAO.add(contract);
        }
    }

    @Override
    public List<Contract> getContracts() {
        return contractDAO.getContracts();
    }

    @Override
    public List<Contract> getContracts(Person student) {
        return contractDAO.getContracts(student);
    }

    @Override
    public Contract getContracts(int id) {
        return contractDAO.getContract(id);
    }
}
