package com.borischistov.mireapayments.services;

import com.borischistov.mireapayments.daos.api.ISemesterDAO;
import com.borischistov.mireapayments.entities.StudySemester;
import com.borischistov.mireapayments.services.api.ISemesterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Boris Chistov
 * Date: 10.11.13
 * Time: 23:49
 * Comment:
 */
@Service
public class SemesterService implements ISemesterService {

    @Autowired
    private ISemesterDAO semesterDAO;

    @Override
    public List<StudySemester> getSemesters() {
        return semesterDAO.getSemesters();
    }

    @Override
    public StudySemester getSemesters(int id) {
        return semesterDAO.getSemesters(id);
    }

    @Override
    public List<StudySemester> getSemesters(int startYear, int endYear) {
        return semesterDAO.getSemesters(startYear, endYear);
    }

    @Override
    public void save(StudySemester semester) {
        if (semester.getSemesterID() > 0) {
            semesterDAO.update(semester);
        } else if (semester.getSemesterID() == 0) {
            semesterDAO.add(semester);
        }
    }

    @Override
    public void delete(StudySemester semester) {
        semesterDAO.delete(semester);
    }

    @Override
    public List<StudySemester> getSemesters(StudySemester start, StudySemester end) {
        return semesterDAO.getSemesters(start, end);
    }
}
