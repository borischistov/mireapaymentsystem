package com.borischistov.mireapayments.services;

import com.borischistov.mireapayments.daos.api.IPaymentPlanDAO;
import com.borischistov.mireapayments.entities.PaymentPlan;
import com.borischistov.mireapayments.services.api.IPaymentPlanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: b_chistov
 * Date: 25.11.13
 * Time: 10:04
 */
@Service
public class PaymentPlanService implements IPaymentPlanService {

    @Autowired
    private IPaymentPlanDAO paymentPlanDAO;

    @Override
    public List<PaymentPlan> getPlans() {
        return paymentPlanDAO.getPlans();
    }

    @Override
    public PaymentPlan getPlans(int id) {
        return paymentPlanDAO.getPlans(id);
    }

    @Override
    public void save(PaymentPlan plan) {
        if (plan != null) {
            if (plan.getId() > 0) {
                paymentPlanDAO.update(plan);
            } else if (plan.getId() == 0) {
                paymentPlanDAO.save(plan);
            }
        }
    }

    @Override
    public void delete(PaymentPlan plan) {
        paymentPlanDAO.delete(plan);
    }
}
