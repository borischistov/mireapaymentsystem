package com.borischistov.mireapayments.services;

import com.borischistov.mireapayments.daos.api.IInvoiceDAO;
import com.borischistov.mireapayments.entities.Contract;
import com.borischistov.mireapayments.entities.Invoice;
import com.borischistov.mireapayments.services.api.IInvoiceService;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFTable;
import org.apache.poi.xwpf.usermodel.XWPFTableCell;
import org.apache.poi.xwpf.usermodel.XWPFTableRow;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Boris Chistov
 * Date: 07.12.13
 * Time: 18:02
 * Comment:
 */
@Service
public class InvoiceService implements IInvoiceService {

    @Autowired
    private IInvoiceDAO invoiceDAO;

    @Override
    public List<Invoice> getInvoices(Contract contract) {
        return invoiceDAO.getInvoices(contract);
    }

    @Override
    public void save(Invoice invoice) {
        if (invoice.getId() > 0) {
            invoiceDAO.update(invoice);
        } else if (invoice.getId() == 0) {
            invoiceDAO.add(invoice);
        }
    }

    @Override
    public XWPFDocument prepareInvoiceFile(Invoice invoice, XWPFDocument doc) {
        List<XWPFTable> tables = doc.getTables();
        for (XWPFTable table : tables) {
            for (XWPFTableRow row : table.getRows()) {
                for (XWPFTableCell cell : row.getTableCells()) {
                    String param = cell.getText();
                    if (param.equals("XXXSTUDENT_FIO")) {
                        cell.removeParagraph(0);
                        cell.setText(invoice.getContract().getStudent().getLastName() + " "
                                + invoice.getContract().getStudent().getFirstName() + " "
                                + invoice.getContract().getStudent().getSurname());
                    } else if (param.equals("XXXPAYMENT_DETAILS")) {
                        StringBuilder sb = new StringBuilder();
                        sb.append("ОПЛАТА ОБУЧЕНИЯ ЗА");
                        sb.append(" " + invoice.getPlan().getStudySemester().getSemester() + " ");
                        sb.append("СЕМЕСТР ");
                        sb.append(invoice.getPlan().getStudySemester().getStartYear() + " / "
                                + invoice.getPlan().getStudySemester().getEndYear() + " УЧЕБНОГО ГОДА.");
                        sb.append(" ПО ДОГОВОРУ № : " + invoice.getContract().getContractNumber());
                        cell.removeParagraph(0);
                        cell.setText(sb.toString());
                    } else if (param.equals("XXXSUM")) {
                        StringBuilder sb = new StringBuilder();
                        sb.append(invoice.getPlan().getPaymentSum().getAmount());
                        sb.append(" руб.");
                        cell.removeParagraph(0);
                        cell.setText(sb.toString());
                    }
                }
            }
        }

        return doc;
    }
}
