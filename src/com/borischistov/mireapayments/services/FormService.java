package com.borischistov.mireapayments.services;

import com.borischistov.mireapayments.daos.api.IFormDAO;
import com.borischistov.mireapayments.entities.Form;
import com.borischistov.mireapayments.services.api.IFormService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: b_chistov
 * Date: 06.11.13
 * Time: 16:37
 */
@Service
public class FormService implements IFormService {

    @Autowired
    private IFormDAO formDAO;

    @Override
    public Form getForms(int id) {
        return formDAO.getForms(id);
    }

    @Override
    public List<Form> getForms() {
        return formDAO.getForms();
    }

    @Override
    public void saveForm(Form form) {
        if (form.getId() > 0) {
            formDAO.update(form);
        } else if (form.getId() == 0) {
            formDAO.addForm(form);
        }
    }

    @Override
    public void delete(Form form) {
        formDAO.delete(form);
    }
}
