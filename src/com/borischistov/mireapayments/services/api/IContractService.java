package com.borischistov.mireapayments.services.api;

import com.borischistov.mireapayments.entities.Contract;
import com.borischistov.mireapayments.entities.Person;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: b_chistov
 * Date: 11.11.13
 * Time: 17:36
 */
public interface IContractService {

    public void save(Contract contract);

    public List<Contract> getContracts();

    public List<Contract> getContracts(Person student);

    public Contract getContracts(int id);

}
