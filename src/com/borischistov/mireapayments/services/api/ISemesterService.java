package com.borischistov.mireapayments.services.api;

import com.borischistov.mireapayments.entities.StudySemester;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Boris Chistov
 * Date: 10.11.13
 * Time: 23:49
 * Comment:
 */
public interface ISemesterService {

    public List<StudySemester> getSemesters();

    public StudySemester getSemesters(int id);

    public List<StudySemester> getSemesters(int startYear, int endYear);

    public List<StudySemester> getSemesters(StudySemester start, StudySemester end);

    public void save(StudySemester semester);

    public void delete(StudySemester semester);
}
