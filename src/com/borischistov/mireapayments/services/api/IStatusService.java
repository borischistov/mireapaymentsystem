package com.borischistov.mireapayments.services.api;

import com.borischistov.mireapayments.entities.Status;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: b_chistov
 * Date: 07.11.13
 * Time: 16:28
 */
public interface IStatusService {
    public Status getStatuses(int id);

    public List<Status> getStatuses();

    public void save(Status status);

    public void delete(Status status);
}
