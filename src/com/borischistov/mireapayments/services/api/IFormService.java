package com.borischistov.mireapayments.services.api;

import com.borischistov.mireapayments.entities.Form;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: b_chistov
 * Date: 06.11.13
 * Time: 16:36
 */
public interface IFormService {

    public Form getForms(int id);

    public List<Form> getForms();

    public void saveForm(Form form);

    public void delete(Form form);
}
