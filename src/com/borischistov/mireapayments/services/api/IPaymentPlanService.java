package com.borischistov.mireapayments.services.api;

import com.borischistov.mireapayments.entities.PaymentPlan;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: b_chistov
 * Date: 25.11.13
 * Time: 9:50
 */
public interface IPaymentPlanService {

    public List<PaymentPlan> getPlans();

    public PaymentPlan getPlans(int id);

    public void save(PaymentPlan plan);

    public void delete(PaymentPlan plan);

}
