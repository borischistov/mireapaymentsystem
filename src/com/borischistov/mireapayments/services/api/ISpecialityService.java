package com.borischistov.mireapayments.services.api;

import com.borischistov.mireapayments.entities.Speciality;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: b_chistov
 * Date: 07.11.13
 * Time: 15:12
 */
public interface ISpecialityService {

    public Speciality getSpecialities(int id);

    public List<Speciality> getSpecialities();

    public void saveSpeciality(Speciality speciality);

    public void delete(Speciality speciality);
}