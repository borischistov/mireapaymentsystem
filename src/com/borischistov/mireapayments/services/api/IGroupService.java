package com.borischistov.mireapayments.services.api;

import com.borischistov.mireapayments.entities.Group;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Boris Chistov
 * Date: 07.11.13
 * Time: 22:23
 * Comment:
 */
public interface IGroupService {

    public Group getGroups(int id);

    public List<Group> getGroups();

    public void save(Group group);

    public void delete(Group group);
}
