package com.borischistov.mireapayments.services.api;

import com.borischistov.mireapayments.entities.Student;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Boris Chistov
 * Date: 04.11.13
 * Time: 20:14
 * Comment:
 */
public interface IStudentService {

    public Student getStudents(int id);

    public Student getStudents(String cardNumber, String recordBookNumber);

    public List<Student> getStudents();

    public void saveStudent(Student student);
}
