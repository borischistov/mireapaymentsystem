package com.borischistov.mireapayments.services.api;

import com.borischistov.mireapayments.entities.utilis.AuthDetails;

/**
 * Created with IntelliJ IDEA.
 * User: b_chistov
 * Date: 28.11.13
 * Time: 14:50
 */
public interface IAuthDetailsService {

    public AuthDetails getAuthDetails(String login, String password);

    public void save(AuthDetails details, boolean isNew);

}
