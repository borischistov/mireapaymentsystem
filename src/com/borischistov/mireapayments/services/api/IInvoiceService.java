package com.borischistov.mireapayments.services.api;

import com.borischistov.mireapayments.entities.Contract;
import com.borischistov.mireapayments.entities.Invoice;
import org.apache.poi.xwpf.usermodel.XWPFDocument;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Boris Chistov
 * Date: 07.12.13
 * Time: 18:02
 * Comment:
 */
public interface IInvoiceService {

    public List<Invoice> getInvoices(Contract contract);

    public XWPFDocument prepareInvoiceFile(Invoice invoice, XWPFDocument doc);

    public void save(Invoice invoice);
}
