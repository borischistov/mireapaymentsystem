package com.borischistov.mireapayments.services;

import com.borischistov.mireapayments.daos.api.ISpecialityDAO;
import com.borischistov.mireapayments.entities.Speciality;
import com.borischistov.mireapayments.services.api.ISpecialityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: b_chistov
 * Date: 07.11.13
 * Time: 15:13
 */
@Service
public class SpecialityService implements ISpecialityService {


    @Autowired
    private ISpecialityDAO specialityDAO;

    @Override
    public Speciality getSpecialities(int id) {
        return specialityDAO.getSpecialities(id);
    }

    @Override
    public List<Speciality> getSpecialities() {
        return specialityDAO.getSpecialities();
    }

    @Override
    public void saveSpeciality(Speciality speciality) {
        if (speciality.getId() > 0) {
            specialityDAO.updateSpeciality(speciality);
        } else if (speciality.getId() == 0) {
            specialityDAO.addSpeciality(speciality);
        }
    }

    @Override
    public void delete(Speciality speciality) {
        specialityDAO.deleteSpeciality(speciality);
    }
}
