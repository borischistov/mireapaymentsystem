package com.borischistov.mireapayments.services;

import com.borischistov.mireapayments.daos.api.IGroupDAO;
import com.borischistov.mireapayments.entities.Group;
import com.borischistov.mireapayments.services.api.IGroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Boris Chistov
 * Date: 07.11.13
 * Time: 22:24
 * Comment:
 */
@Service
public class GroupService implements IGroupService {

    @Autowired
    private IGroupDAO groupDAO;

    @Override
    public Group getGroups(int id) {
        return groupDAO.getGroups(id);
    }

    @Override
    public List<Group> getGroups() {
        return groupDAO.getGroups();
    }

    @Override
    public void save(Group group) {
        if (group.getId() > 0) {
            groupDAO.update(group);
        } else if (group.getId() == 0) {
            groupDAO.add(group);
        }
    }

    @Override
    public void delete(Group group) {
        groupDAO.delete(group);
    }
}
