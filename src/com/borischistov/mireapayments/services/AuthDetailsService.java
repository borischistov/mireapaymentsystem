package com.borischistov.mireapayments.services;

import com.borischistov.mireapayments.daos.api.IAuthDetailsDAO;
import com.borischistov.mireapayments.entities.utilis.AuthDetails;
import com.borischistov.mireapayments.services.api.IAuthDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

/**
 * Created with IntelliJ IDEA.
 * User: b_chistov
 * Date: 28.11.13
 * Time: 14:52
 */
@Service
public class AuthDetailsService implements IAuthDetailsService, UserDetailsService {

    @Autowired
    private IAuthDetailsDAO authDetailsDAO;

    @Override
    public AuthDetails getAuthDetails(String login, String password) {
        return authDetailsDAO.getAuthDetails(login, password);
    }

    @Override
    public void save(AuthDetails details, boolean isNew) {
        if (details != null) {
            if (!isNew) {
                authDetailsDAO.updateAuthDetails(details);
            } else {
                authDetailsDAO.addAuthDetails(details);
            }
        }
    }

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        AuthDetails result = authDetailsDAO.getAuthDetails(s);
        return result;
    }
}
