package com.borischistov.mireapayments.services;

import com.borischistov.mireapayments.daos.api.IStudentDAO;
import com.borischistov.mireapayments.entities.Student;
import com.borischistov.mireapayments.entities.utilis.AuthDetails;
import com.borischistov.mireapayments.services.api.IAuthDetailsService;
import com.borischistov.mireapayments.services.api.IStudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Boris Chistov
 * Date: 04.11.13
 * Time: 20:16
 * Comment:
 */
@Service
public class StudentService implements IStudentService {

    @Autowired
    private IStudentDAO studentDAO;
    @Autowired
    private IAuthDetailsService authDetailsService;


    @Override
    public Student getStudents(String cardNumber, String recordBookNumber) {
        return studentDAO.getStudents(cardNumber, recordBookNumber);
    }

    @Override
    public Student getStudents(int id) {
        return studentDAO.getStudents(id);
    }

    @Override
    public List<Student> getStudents() {
        return studentDAO.getStudents();
    }

    @Override
    public void saveStudent(Student student) {
        if (student.getId() > 0) {
            studentDAO.update(student);
            authDetailsService.save(new AuthDetails(student.getCardNumber(), student.getRecordBookNumber(), "ROLE_STUDENT", student), false);
        } else if (student.getId() == 0) {
            studentDAO.addStudent(student);
            authDetailsService.save(new AuthDetails(student.getCardNumber(), student.getRecordBookNumber(), "ROLE_STUDENT", student), true);
        }


    }
}
